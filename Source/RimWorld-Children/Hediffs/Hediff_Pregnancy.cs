﻿namespace RimWorldChildren {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using RimWorld;
    using UnityEngine;
    using Verse;
    using RimWorldChildren.API;

    public abstract class Hediff_Pregnancy : HediffWithComps {
        public Pawn father;
        protected bool isDiscovered = false;
        protected List<Pawn> babies = new List<Pawn>();
        protected bool successfulBirth = true;

        public override void ExposeData() {
            base.ExposeData();
            Scribe_References.Look(ref father, "father", false);
            Scribe_Values.Look(ref isDiscovered, "is_discovered", false, false);
            Scribe_Collections.Look(ref babies, true, "babies", LookMode.Deep, ctorArgs: new object[0]);
        }

        /// <summary>
        /// Return by value a collection babies waiting to be born
        /// </summary>
        public List<Pawn> Babies {
            get => new List<Pawn>(babies);
        }

        /// <summary>
        /// Gets the current progress of this pregnancy
        /// </summary>
        public float GestationProgress {
            get {
                return Severity;
            }

            private set {
                Severity = value;
            }
        }

        public static Hediff_Pregnancy Create<T>(Pawn mother, Pawn father) {
            var torso = mother.RaceProps.body.AllParts.Find(x => x.def == BodyPartDefOf.Torso);
            Hediff_Pregnancy hediff = (Hediff_Pregnancy)HediffMaker.MakeHediff(HediffDef.Named(PregnancyUtility.HediffOfClass[typeof(T)]), mother, torso);
            hediff.isDiscovered = false;

            // Todo: Capture genetics info about father/mother and save on hediff instead of ref.
            // If father leaves or is killed, info is still accessible when child is eventually born
            hediff.father = father;
            hediff.MakeBabies();
            CLog.Message("Generated " + hediff.babies.Count + " babies.");
            hediff.PostImpregnate();
            mother.health.AddHediff(hediff);
            return hediff;
        }

        /// <summary>
        /// Actions to be executed immediately following impregnation.
        /// </summary>
        protected virtual void PostImpregnate() {
            LessonAutoActivator.TeachOpportunity(CnpConceptDefOf.CnpMiscarriage, OpportunityType.GoodToKnow);
        }

        /// <summary>
        /// Create a number of babies to be born, eventually
        /// </summary>
        protected virtual void MakeBabies() {
            int babiesToMake = (pawn.RaceProps.litterSizeCurve == null) ? 1 : Mathf.RoundToInt(Rand.ByCurve(pawn.RaceProps.litterSizeCurve));
            if (babiesToMake < 1) {
                babiesToMake = 1;
            }

            // Make sure the pawn looks like mommy and daddy
            float skin_whiteness = Rand.Range(0, 1);
            // Pool of "genetic traits" the baby can inherit
            TraitPool traitpool = new TraitPool();

            if (pawn.RaceProps.Humanlike) {
                // Add mom's traits to the pool
                foreach (Trait momtrait in pawn.story.traits.allTraits) {
                    traitpool.Add(new AcquirableTrait(momtrait.def, momtrait.Degree));
                }

                if (father != null) {
                    // Add dad's traits to the pool
                    foreach (Trait dadtrait in father.story.traits.allTraits) {
                        traitpool.Add(new AcquirableTrait(dadtrait.def, dadtrait.Degree));
                    }

                    // If baby is inbred, add Staggeringly Ugly trait to the pool
                    if (pawn.relations.FamilyByBlood.Contains(father) && Rand.Bool)
                    {
                        // Remove all other beauty traits
                        foreach (AcquirableTrait a in traitpool.Where(a => a.TraitDef == TraitDefOf.Beauty)) {
                            traitpool.Remove(a);
                        }

                        traitpool.Add(new AcquirableTrait(TraitDefOf.Beauty, -2));
                    }

                    // Blend skin colour between mom and dad
                    skin_whiteness = (pawn.story.melanin + father.story.melanin) / 2;
                }
                else {
                    // If dad doesn't exist, just use mom's skin colour
                    skin_whiteness = pawn.story.melanin;
                }

                // Clear out any traits that aren't genetic from the list
                traitpool.Where(trait => !PregnancyUtility.GetGeneticTraits().Contains(trait.TraitDef)).ToList().ForEach(trait => traitpool.Remove(trait));
            }

            // Surname passing
            string last_name = null;
            if (pawn.RaceProps.Humanlike) {
                if (father == null) {
                    last_name = NameTriple.FromString(pawn.Name.ToStringFull).Last;
                }
                else {
                    last_name = NameTriple.FromString(father.Name.ToStringFull).Last;
                }

                Pawn baby;
                PawnGenerationRequest request = new PawnGenerationRequest(pawn.kindDef, pawn.Faction, PawnGenerationContext.NonPlayer, pawn.Map.Tile, true, true, false, false, false, false, 0f, false, true, false, false, false, false, false, false, 0f, null, 0f, null, null, null, null, 0f, 0f, 0f, default(Gender?), skin_whiteness, last_name, null, null);
                request.Newborn = true;

                for (int i = 0; i < babiesToMake; i++) {
                    baby = PawnGenerator.GeneratePawn(request);
                    baby.story.traits.allTraits.Clear();
                    baby.story.adulthood = null;

                    if (baby.TryGetComp<LifecycleComp>() != null) {
                        LifecycleComp comp = baby.TryGetComp<LifecycleComp>();
                        comp.Props.ColonyBorn = true;
                        comp.Props.geneticTraits = traitpool;
                        comp.Initialize(true);
                    }

                    babies.Add(baby);
                }
            }

        }

        /// <summary>
        /// Destroy or dispose of a baby in whatever way makes sense for this pregnancy/race
        /// </summary>
        /// <param name="baby">one baby to be destroyed</param>
        protected virtual void DestroyBaby(Pawn baby) {
        }

        /// <summary>
        /// Miscarry a specified baby
        /// </summary>
        /// <param name="baby">a Pawn from the babies collection to be disposed of</param>
        public virtual void Miscarry(Pawn baby) {
            if (babies.Contains(baby)) {
                DestroyBaby(baby);
            }
            else {
                CLog.Error("Attempted to miscarry a baby that does not belong to " + pawn.Name);
            }
            PostMiscarry(baby);
        }

        protected virtual void PostMiscarry(Pawn baby) {

        }

        /// <summary>
        /// Abort a specified baby
        /// </summary>
        /// <param name="baby">a Pawn from the babies collection to be disposed of</param>
        public virtual void Abort(Pawn baby) {
            if (babies.Contains(baby)) {
                DestroyBaby(baby);
            }
            else {
                CLog.Error("Attempted to abort a baby that does not belong to " + pawn.Name);
            }
            PostAbort(baby);
        }

        protected virtual void PostAbort(Pawn baby) {

        }

        /// <summary>
        /// Whether or not the pregnancy hediff has been noticed by the pawns
        /// </summary>
        public override bool Visible {
            get => isDiscovered;
        }

        /// <summary>
        /// Attempt to discover the pregnancy
        /// </summary>
        /// <returns>true if the pregnancy is determined by the pawns</returns>
        public virtual bool TryDiscover() {
            if ((pawn.story.bodyType == BodyTypeDefOf.Female && GestationProgress > 0.389f)
                    || (pawn.story.bodyType == BodyTypeDefOf.Thin && GestationProgress > 0.375f)
                    || ((pawn.story.bodyType == BodyTypeDefOf.Fat || pawn.story.bodyType == BodyTypeDefOf.Hulk)
                    && GestationProgress > 0.50f)) {
                // Got the numbers by dividing the average show time (in weeks) per body type by 36
                // (36 weeks being the real human gestation period)
                // https://www.momtricks.com/pregnancy/when-do-you-start-showing-in-pregnancy/
                DiscoverPregnancy("WordHumanPregnancy", "MessageIsPregnant");
                return true;
            }
            return false;
        }

        /// <summary>
        /// Discover the pregnancy and alert the player. The hediff is now visible.
        /// PostDiscoverPregnancy will be executed afterwards.
        /// </summary>
        /// <param name="messageTitleKey">A message/label key indicating the translated message bundle record to use for the message title</param>
        /// <param name="messageKey">A message/label key indicating the translated message bundle record to use for the message body</param>
        public virtual void DiscoverPregnancy(string messageTitleKey, string messageKey) {
            isDiscovered = true;
            Find.LetterStack.ReceiveLetter(messageTitleKey.Translate(), TranslatorFormattedStringExtensions.Translate(messageKey, pawn.LabelIndefinite()), LetterDefOf.PositiveEvent, pawn);
            PostDiscoverPregnancy();
        }

        /// <summary>
        /// Actions to be executed once the pregnancy has been discovered and announced to the player.
        /// This will allow implementing classes to interface with the discovery for things like
        /// memory/thought manipulation or other things.
        /// </summary>
        public virtual void PostDiscoverPregnancy() {

        }

        public override void PostMake() {
            // Ensure the hediff always applies to the torso, regardless of incorrect directive
            base.PostMake();
            var torso = pawn.RaceProps.body.AllParts.Find(x => x.def.defName == "Torso");
            if (Part != torso) {
                Part = torso;
            }
        }

        /// <summary>
        /// This method is executed immediately before the pawn gives birth and babies are handled.
        /// </summary>
        protected virtual void PreBirth() {

        }

        /// <summary>
        /// Handle birth for all babies
        /// </summary>
        /// /// <param name="baby">The baby to be born</param>
        /// <param name="chance_successful"> The base chance of a successful delivery</param>
        protected abstract void Birth(Pawn baby, float chance_successful = 1.0f);

        /// <summary>
        /// This method is executed immediately after the pawn gives birth, if the birth succeeded.
        /// This method is assumed to be called once per baby born
        /// </summary>
        /// <param name="baby">The newly birthed baby</param>
        protected virtual void PostBirth(Pawn baby) {

            // Set baby chronological age to 0
            baby.ageTracker.AgeChronologicalTicks = 0;
            if (pawn.IsPrisonerOfColony) {
                baby.guest.SetGuestStatus(Faction.OfPlayer, true);
            }
            LessonAutoActivator.TeachOpportunity(CnpConceptDefOf.CnpAcquiringTraits, OpportunityType.GoodToKnow);
        }

        /// <summary>
        /// This method will be executed once after all birthing has been completed
        /// </summary>
        protected virtual void PostBirth() {

        }

        /// <summary>
        /// Actual handling of birth. This method will serves as a driver to handle pre, birth, and post.
        ///
        /// </summary>
        public virtual void ProcessBirths() {
            // Has the pregnancy been tended to?
            if (pawn.health.hediffSet.GetFirstHediffOfDef(HediffDef.Named("GivingBirth")).IsTended()) {
                PreBirth();
                CLog.Message(pawn.Name + "'s labor is tended");

                // Then we get a safe pregnancy
                foreach (Pawn baby in babies) {
                    Birth(baby, 1);
                    PostBirth(baby);
                }

                PostBirth();
                Destroy();

            }
            else if (GestationProgress >= 1) {
                // natural birth (probably not a good idea!)
                PreBirth();
                CLog.Message(pawn.Name + "'s labor is untended");

                // Do risky pregnancy
                foreach (Pawn baby in babies) {
                    Birth(baby, 0.9f);
                    PostBirth(baby);
                }

                if (Rand.Value <= 0.1f) {
                    pawn.health.AddHediff(HediffDef.Named("PlacentaBleed"), ChildrenUtility.GetPawnBodyPart(pawn, "Torso"), null);
                }

                PostBirth();
                Destroy();
            }
        }

        public override void Tick() {
            // Something has gone horribly wrong
            if (pawn.health.hediffSet.HasHediff(HediffDef.Named("PostPregnancy"))) {
                CLog.Error("HumanPregnancy Hediff was not properly removed when pawn " + pawn.Name.ToStringShort + " gave birth.");
                if (pawn.health.hediffSet.HasHediff(HediffDef.Named("GivingBirth"))) {
                    pawn.health.RemoveHediff(pawn.health.hediffSet.GetFirstHediffOfDef(HediffDef.Named("GivingBirth")));
                }

                Destroy();
            }

            ageTicks++;

            // Advance the gestation
            GestationProgress += 1.0f / (pawn.TryGetComp<LifecycleComp>().LifecycleDef.gestationDays * GenDate.TicksPerDay);

            // Check if pregnancy is far enough along to "show" for the body type
            if (!isDiscovered) {
                TryDiscover();
            }

            // Final stage of pregnancy
            if (CurStageIndex == def.stages.Count - 1) {
                if (!pawn.health.hediffSet.HasHediff(HediffDef.Named("GivingBirth"))) {
                    // Notify the player birth is near
                    if (Visible && PawnUtility.ShouldSendNotificationAbout(pawn)) {
                        Messages.Message("MessageHavingContractions".Translate(pawn.LabelIndefinite()).CapitalizeFirst(), pawn, MessageTypeDefOf.NeutralEvent);
                    }

                    // Give the mother the GivingBirth hediff
                    pawn.health.AddHediff(HediffDef.Named("GivingBirth"), ChildrenUtility.GetPawnBodyPart(pawn, "Torso"), null);
                }
                else {
                    // We're having contractions now
                    ProcessBirths();
                }
            }
        }

        /// <summary>
        /// Determines whether the pregnancy is considered mostly-complete
        /// </summary>
        /// <returns>true if gestation progress is > 75%</returns>
        public bool IsLateTerm() {
            return GestationProgress > 0.75f;
        }

        /// <summary>
        /// Tear down and remove this hediff, ending the pregnancy
        /// </summary>
        public virtual void Destroy() {
            CLog.Message("Destroying pregnancy for " + pawn.Name);
            if (pawn.health.hediffSet.HasHediff(HediffDef.Named("GivingBirth"))) {
                pawn.health.RemoveHediff(pawn.health.hediffSet.GetFirstHediffOfDef(HediffDef.Named("GivingBirth")));
            }

            pawn.health.RemoveHediff(this);
        }

        public override string DebugString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(base.DebugString());
            stringBuilder.AppendLine("Gestation progress: " + GestationProgress.ToStringPercent());
            stringBuilder.AppendLine("Time left: " + ((int)((1 - GestationProgress) * pawn.TryGetComp<LifecycleComp>().LifecycleDef.gestationDays * GenDate.TicksPerDay)).ToStringTicksToPeriod());
            return stringBuilder.ToString();
        }

    }
}
