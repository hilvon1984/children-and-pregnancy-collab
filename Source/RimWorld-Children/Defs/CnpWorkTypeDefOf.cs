﻿using RimWorld;
using Verse;

namespace RimWorldChildren {

    /// <summary>
    /// Def lib for CNP work types
    /// </summary>
    [DefOf]
    public class CnpWorkTypeDefOf {
        public static WorkTypeDef Childcare;

        static CnpWorkTypeDefOf() {
            DefOfHelper.EnsureInitializedInCtor(typeof(CnpWorkTypeDefOf));
        }
    }
}
