﻿using RimWorld;

namespace RimWorldChildren {
    /// <summary>
    /// Static accessors for CNP concepts
    /// </summary>
    [DefOf]
    public static class CnpConceptDefOf {
        public static ConceptDef CnpMiscarriage;
        public static ConceptDef CnpBabyCarry;
        public static ConceptDef CnpAdoption;
        public static ConceptDef CnpAcquiringTraits;

        static CnpConceptDefOf() {
            DefOfHelper.EnsureInitializedInCtor(typeof(CnpConceptDefOf));
        }

    }
}
