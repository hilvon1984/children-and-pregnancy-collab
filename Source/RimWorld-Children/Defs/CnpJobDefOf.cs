﻿using RimWorld;
using Verse;

namespace RimWorldChildren {

    /// <summary>
    /// Def lib for CNP work types
    /// </summary>
    [DefOf]
    public class CnpJobDefOf {
        public static JobDef DisciplineChild;
        public static JobDef ScoldChild;
        public static JobDef BreastfeedBaby;
        public static JobDef FoodFeedBaby;
        public static JobDef TakeBabyToBedAndFeed;
        public static JobDef TakeBabyToCrib;
        public static JobDef PlayWithBaby;
        public static JobDef AdoptBaby;
        public static JobDef CarryBaby;
        public static JobDef Carried;

        static CnpJobDefOf() {
            DefOfHelper.EnsureInitializedInCtor(typeof(CnpJobDefOf));
        }
    }
}
