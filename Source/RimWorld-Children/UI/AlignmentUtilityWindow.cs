﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;

namespace RimWorldChildren {
    /// <summary>
    /// Defines a utility window to be used for aligning body parts when building defs.
    /// Note that this tool is only intended to be used for development of racial patches.
    /// It is not intended to be used as an in-game def editor. This is purely a development tool.
    /// </summary>
    class AlignmentUtilityWindow : Window{

        private static float storedX;
        private static float storedY;
        private static List<string> bodyParts;
        private static string selectedPart;
        private static int currentLifestage;
        private static Rot4 currentDirection;
        private static List<Rot4> directions;
        private static List<FloatMenuOption> directionsList;
        private static List<FloatMenuOption> lifestageIndexList;
        private static List<FloatMenuOption> partsList;
        private static List<FloatMenuOption> defsList;
        private static RacialLifestageDef def;
        private static string currentDef;

        public AlignmentUtilityWindow() : base() {
            doCloseButton = false;
            layer = WindowLayer.GameUI;
            closeOnCancel = true;
            doCloseX = true;
            absorbInputAroundWindow = false;
            closeOnClickedOutside = false;
            preventCameraMotion = false;
            doWindowBackground = true;
            forcePause = false;
            resizeable = false;
            draggable = true;
            shadowAlpha = 50;
            currentLifestage = 0;
            directions = new List<Rot4>() { Rot4.South, Rot4.North, Rot4.East, Rot4.West };
            currentDef = "Default_Lifestages";
            def = DefDatabase<RacialLifestageDef>.GetNamed(currentDef);
            currentDirection = directions[0];
            bodyParts = def.lifeStages[2]?.graphics?.bodyAddons?.Select(bp => bp.bodyPart).ToList();
            selectedPart = bodyParts[0];
            directionsList = new List<FloatMenuOption>();
            defsList = new List<FloatMenuOption>();

            foreach (Rot4 dir in directions) {
                directionsList.Add(new FloatMenuOption(dir.ToStringWord(), () => {
                    currentDirection = dir;
                }, MenuOptionPriority.Default));
            }

            foreach (RacialLifestageDef raceDef in DefDatabase<RacialLifestageDef>.AllDefsListForReading) {
                defsList.Add(new FloatMenuOption(raceDef.defName, () => {
                    currentDef = raceDef.defName;
                    def = DefDatabase<RacialLifestageDef>.GetNamed(currentDef);
                    bodyParts = def.lifeStages[2]?.graphics?.bodyAddons?.Select(bp => bp.bodyPart).ToList();
                    selectedPart = bodyParts[0];
                    RefreshDefOptions();
                }, MenuOptionPriority.Default));
            }

            RefreshDefOptions();
        }

        /// <summary>
        /// These menus must be refreshed when the def is changed
        /// </summary>
        private static void RefreshDefOptions() {
            List<int> stages = new List<int>();
            lifestageIndexList = new List<FloatMenuOption>();
            partsList = new List<FloatMenuOption>();
            for (int i = 0; i < def.lifeStages.Count; ++i) {
                stages.Add(i);
            }

            foreach (int stage in stages) {
                lifestageIndexList.Add(new FloatMenuOption(stage.ToString(), () => {
                    currentLifestage = stage;
                }, MenuOptionPriority.Default));
            }

            foreach (string part in bodyParts) {
                partsList.Add(new FloatMenuOption(part, () => {
                    selectedPart = part;
                }, MenuOptionPriority.Default));
            }
        }

        public override void DoWindowContents(Rect inRect) {
            storedX = windowRect.x;
            storedY = windowRect.y;
            GUI.BeginGroup(inRect);
            Text.Font = GameFont.Tiny;
            Rect margin = new Rect(0.0f, 20f, inRect.width, inRect.height - 20f).ContractedBy(10f);
            Rect settingsSpace = margin;
            GUI.color = Color.white;
            float selectorHeight = 22f;
            DrawSelectors(new Rect(5f, 5f, inRect.width, selectorHeight));
            settingsSpace.y += selectorHeight;
            DrawSettings(settingsSpace);
            Text.Anchor = TextAnchor.UpperLeft;
            GUI.EndGroup();
        }

        private void DrawSettings(Rect rect) {
            GUI.BeginGroup(rect);
            Widgets.Label(new Rect(20, 0.0f, 100, 22f), "Offset: ");
            if (Widgets.ButtonText(new Rect(20f, 20.0f, 20, 20), "--x")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).offset.x -= 0.025f;
            }

            if (Widgets.ButtonText(new Rect(40f, 20.0f, 20, 20), "-x")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).offset.x -= 0.001f;
            }

            Widgets.Label(new Rect(70, 20.0f, 50, 22f), def.lifeStages[currentLifestage]?.graphics?.bodyAddons?.Find(x => x.bodyPart == selectedPart)?.GetDirectionalAdjuster(currentDirection)?.offset.x.ToString("0.000") ?? "NA");
            if (Widgets.ButtonText(new Rect(130, 20.0f, 20, 20), "+x")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).offset.x += 0.001f;
            }

            if (Widgets.ButtonText(new Rect(150, 20.0f, 20, 20), "++x")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).offset.x += 0.025f;
            }

            if (Widgets.ButtonText(new Rect(20f, 50.0f, 20, 20), "--y")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).offset.y -= 0.025f;
            }

            if (Widgets.ButtonText(new Rect(40f, 50.0f, 20, 20), "-y")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).offset.y -= 0.001f;
            }

            Widgets.Label(new Rect(70, 50.0f, 50, 22f), def.lifeStages[currentLifestage]?.graphics?.bodyAddons?.Find(x => x.bodyPart == selectedPart)?.GetDirectionalAdjuster(currentDirection)?.offset.y.ToString("0.000") ?? "NA");
            if (Widgets.ButtonText(new Rect(130, 50.0f, 20, 20), "+y")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).offset.y += 0.001f;
            }

            if (Widgets.ButtonText(new Rect(150, 50.0f, 20, 20), "++y")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).offset.y += 0.025f;
            }

            Widgets.Label(new Rect(200, 0.0f, 100, 22f), "Scale: ");
            if (Widgets.ButtonText(new Rect(200f, 20.0f, 20, 20), "--x")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).scale.x -= 0.025f;
            }

            if (Widgets.ButtonText(new Rect(220f, 20.0f, 20, 20), "-x")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).scale.x -= 0.001f;
            }

            Widgets.Label(new Rect(250, 20.0f, 50, 22f), def.lifeStages[currentLifestage]?.graphics?.bodyAddons?.Find(x => x.bodyPart == selectedPart)?.GetDirectionalAdjuster(currentDirection)?.scale.x.ToString("0.000") ?? "NA");
            if (Widgets.ButtonText(new Rect(310, 20.0f, 20, 20), "+x")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).scale.x += 0.001f;
            }

            if (Widgets.ButtonText(new Rect(330, 20.0f, 20, 20), "++x")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).scale.x += 0.025f;
            }

            if (Widgets.ButtonText(new Rect(200f, 50.0f, 20, 20), "--y")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).scale.y -= 0.025f;
            }

            if (Widgets.ButtonText(new Rect(220f, 50.0f, 20, 20), "-y")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).scale.y -= 0.001f;
            }

            Widgets.Label(new Rect(250f, 50.0f, 50, 22f), def.lifeStages[currentLifestage]?.graphics?.bodyAddons?.Find(x => x.bodyPart == selectedPart)?.GetDirectionalAdjuster(currentDirection)?.scale.y.ToString("0.000") ?? "NA");

            if (Widgets.ButtonText(new Rect(310f, 50.0f, 20, 20), "+y")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).scale.y += 0.001f;
            }

            if (Widgets.ButtonText(new Rect(330f, 50.0f, 20, 20), "++y")) {
                def.lifeStages[currentLifestage].graphics.bodyAddons.Find(x => x.bodyPart == selectedPart).GetDirectionalAdjuster(currentDirection).scale.y += 0.025f;
            }

            if (Widgets.ButtonText(new Rect(20f, 80.0f, 200, 20), "head " + def.lifeStages[currentLifestage].graphics.renderHead)) {
                def.lifeStages[currentLifestage].graphics.renderHead = !def.lifeStages[currentLifestage].graphics.renderHead;
            }


            Widgets.Label(new Rect(20f, 100.0f, 100, 22f), "Hair Offset: ");
            if (Widgets.ButtonText(new Rect(20f, 120.0f, 20, 20), "--x")) {
                def.lifeStages[currentLifestage].graphics.hairOffset.x -= 0.025f;
            }

            if (Widgets.ButtonText(new Rect(40f, 120.0f, 20, 20), "-x")) {
                def.lifeStages[currentLifestage].graphics.hairOffset.x -= 0.001f;
            }

            Widgets.Label(new Rect(70f, 120.0f, 50, 22f), def.lifeStages[currentLifestage]?.graphics?.hairOffset.x.ToString("0.000") ?? "NA");
            if (Widgets.ButtonText(new Rect(130, 120.0f, 20, 20), "+x")) {
                def.lifeStages[currentLifestage].graphics.hairOffset.x += 0.001f;
            }

            if (Widgets.ButtonText(new Rect(150, 120.0f, 20, 20), "++x")) {
                def.lifeStages[currentLifestage].graphics.hairOffset.x += 0.025f;
            }

            if (Widgets.ButtonText(new Rect(20f, 150.0f, 20, 20), "--y")) {
                def.lifeStages[currentLifestage].graphics.hairOffset.y -= 0.025f;
            }

            if (Widgets.ButtonText(new Rect(40f, 150.0f, 20, 20), "-y")) {
                def.lifeStages[currentLifestage].graphics.hairOffset.y -= 0.001f;
            }

            Widgets.Label(new Rect(70f, 150.0f, 50, 22f), def.lifeStages[currentLifestage]?.graphics?.hairOffset.y.ToString("0.000") ?? "NA");

            if (Widgets.ButtonText(new Rect(130f, 150.0f, 20, 20), "+y")) {
                def.lifeStages[currentLifestage].graphics.hairOffset.y += 0.001f;
            }

            if (Widgets.ButtonText(new Rect(150f, 150.0f, 20, 20), "++y")) {
                def.lifeStages[currentLifestage].graphics.hairOffset.y += 0.025f;
            }


            Widgets.Label(new Rect(200, 100.0f, 100, 22f), "Hair Scale: ");
            if (Widgets.ButtonText(new Rect(200f, 120.0f, 20, 20), "--x")) {
                def.lifeStages[currentLifestage].graphics.hairScale.x -= 0.025f;
                UpdateMeshsets();
            }

            if (Widgets.ButtonText(new Rect(220f, 120.0f, 20, 20), "-x")) {
                def.lifeStages[currentLifestage].graphics.hairScale.x -= 0.001f;
            }

            Widgets.Label(new Rect(250, 120.0f, 50f, 22f), def.lifeStages[currentLifestage]?.graphics?.hairScale.x.ToString("0.000") ?? "NA");
            if (Widgets.ButtonText(new Rect(310, 120.0f, 20, 20), "+x")) {
                def.lifeStages[currentLifestage].graphics.hairScale.x += 0.001f;
                UpdateMeshsets();
            }

            if (Widgets.ButtonText(new Rect(330, 120.0f, 20, 20), "++x")) {
                def.lifeStages[currentLifestage].graphics.hairScale.x += 0.025f;
                UpdateMeshsets();
            }

            if (Widgets.ButtonText(new Rect(200f, 150.0f, 20, 20), "--y")) {
                def.lifeStages[currentLifestage].graphics.hairScale.y -= 0.025f;
                UpdateMeshsets();
            }

            if (Widgets.ButtonText(new Rect(220f, 150.0f, 20, 20), "-y")) {
                def.lifeStages[currentLifestage].graphics.hairScale.y -= 0.001f;
                UpdateMeshsets();
            }

            Widgets.Label(new Rect(250f, 150.0f, 50, 22f), def.lifeStages[currentLifestage]?.graphics?.hairScale.y.ToString("0.000") ?? "NA");

            if (Widgets.ButtonText(new Rect(310f, 150.0f, 20, 20), "+y")) {
                def.lifeStages[currentLifestage].graphics.hairScale.y += 0.001f;
                UpdateMeshsets();
            }

            if (Widgets.ButtonText(new Rect(330f, 150.0f, 20, 20), "++y")) {
                def.lifeStages[currentLifestage].graphics.hairScale.y += 0.025f;
                UpdateMeshsets();
            }


            Widgets.Label(new Rect(200, 170.0f, 100, 22f), "Head Scale: ");
            if (Widgets.ButtonText(new Rect(200f, 190.0f, 20, 20), "--x")) {
                def.lifeStages[currentLifestage].graphics.headScale.x -= 0.025f;
                UpdateMeshsets();
            }

            if (Widgets.ButtonText(new Rect(220f, 190.0f, 20, 20), "-x")) {
                def.lifeStages[currentLifestage].graphics.headScale.x -= 0.001f;
            }

            Widgets.Label(new Rect(250, 190.0f, 50f, 22f), def.lifeStages[currentLifestage]?.graphics?.headScale.x.ToString("0.000") ?? "NA");
            if (Widgets.ButtonText(new Rect(310, 190.0f, 20, 20), "+x")) {
                def.lifeStages[currentLifestage].graphics.headScale.x += 0.001f;
                UpdateMeshsets();
            }

            if (Widgets.ButtonText(new Rect(330, 190.0f, 20, 20), "++x")) {
                def.lifeStages[currentLifestage].graphics.headScale.x += 0.025f;
                UpdateMeshsets();
            }

            if (Widgets.ButtonText(new Rect(200f, 220.0f, 20, 20), "--y")) {
                def.lifeStages[currentLifestage].graphics.headScale.y -= 0.025f;
                UpdateMeshsets();
            }

            if (Widgets.ButtonText(new Rect(220f, 220.0f, 20, 20), "-y")) {
                def.lifeStages[currentLifestage].graphics.headScale.y -= 0.001f;
                UpdateMeshsets();
            }

            Widgets.Label(new Rect(250f, 220.0f, 50, 22f), def.lifeStages[currentLifestage]?.graphics?.headScale.y.ToString("0.000") ?? "NA");

            if (Widgets.ButtonText(new Rect(310f, 220.0f, 20, 20), "+y")) {
                def.lifeStages[currentLifestage].graphics.headScale.y += 0.001f;
                UpdateMeshsets();
            }

            if (Widgets.ButtonText(new Rect(330f, 220.0f, 20, 20), "++y")) {
                def.lifeStages[currentLifestage].graphics.headScale.y += 0.025f;
                UpdateMeshsets();
            }


            Widgets.Label(new Rect(20f, 170.0f, 100, 22f), "Hat Offset NS: ");
            if (Widgets.ButtonText(new Rect(20f, 190.0f, 20, 20), "--x")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetNS.x -= 0.025f;
            }

            if (Widgets.ButtonText(new Rect(40f, 190.0f, 20, 20), "-x")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetNS.x -= 0.001f;
            }

            Widgets.Label(new Rect(70f, 190.0f, 50, 22f), def.lifeStages[currentLifestage]?.graphics?.hatOffsetNS.x.ToString("0.000") ?? "NA");
            if (Widgets.ButtonText(new Rect(130, 190.0f, 20, 20), "+x")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetNS.x += 0.001f;
            }

            if (Widgets.ButtonText(new Rect(150, 190.0f, 20, 20), "++x")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetNS.x += 0.025f;
            }

            if (Widgets.ButtonText(new Rect(20f, 220.0f, 20, 20), "--y")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetNS.y -= 0.025f;
            }

            if (Widgets.ButtonText(new Rect(40f, 220.0f, 20, 20), "-y")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetNS.y -= 0.001f;
            }

            Widgets.Label(new Rect(70f, 220.0f, 50, 22f), def.lifeStages[currentLifestage]?.graphics?.hatOffsetNS.y.ToString("0.000") ?? "NA");

            if (Widgets.ButtonText(new Rect(130f, 220.0f, 20, 20), "+y")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetNS.y += 0.001f;
            }

            if (Widgets.ButtonText(new Rect(150f, 220.0f, 20, 20), "++y")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetNS.y += 0.025f;
            }


            Widgets.Label(new Rect(20f, 240.0f, 100, 22f), "Hat Offset EW: ");
            if (Widgets.ButtonText(new Rect(20f, 260.0f, 20, 20), "--x")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetEW.x -= 0.025f;
            }

            if (Widgets.ButtonText(new Rect(40f, 260.0f, 20, 20), "-x")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetEW.x -= 0.001f;
            }

            Widgets.Label(new Rect(70f, 260.0f, 50, 22f), def.lifeStages[currentLifestage]?.graphics?.hatOffsetEW.x.ToString("0.000") ?? "NA");
            if (Widgets.ButtonText(new Rect(130, 260.0f, 20, 20), "+x")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetEW.x += 0.001f;
            }

            if (Widgets.ButtonText(new Rect(150, 260.0f, 20, 20), "++x")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetEW.x += 0.025f;
            }

            if (Widgets.ButtonText(new Rect(20f, 290.0f, 20, 20), "--y")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetEW.y -= 0.025f;
            }

            if (Widgets.ButtonText(new Rect(40f, 290.0f, 20, 20), "-y")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetEW.y -= 0.001f;
            }

            Widgets.Label(new Rect(70f, 290.0f, 50, 22f), def.lifeStages[currentLifestage]?.graphics?.hatOffsetEW.y.ToString("0.000") ?? "NA");

            if (Widgets.ButtonText(new Rect(130f, 290.0f, 20, 20), "+y")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetEW.y += 0.001f;
            }

            if (Widgets.ButtonText(new Rect(150f, 290.0f, 20, 20), "++y")) {
                def.lifeStages[currentLifestage].graphics.hatOffsetEW.y += 0.025f;
            }

            Widgets.Label(new Rect(20f, 310.0f, 100, 22f), "Head Offset: ");
            if (Widgets.ButtonText(new Rect(20f, 330.0f, 20, 20), "--x")) {
                def.lifeStages[currentLifestage].graphics.headOffset.x -= 0.025f;
            }

            if (Widgets.ButtonText(new Rect(40f, 330.0f, 20, 20), "-x")) {
                def.lifeStages[currentLifestage].graphics.headOffset.x -= 0.001f;
            }

            Widgets.Label(new Rect(70f, 330.0f, 50, 22f), def.lifeStages[currentLifestage]?.graphics?.headOffset.x.ToString("0.000") ?? "NA");
            if (Widgets.ButtonText(new Rect(130, 330.0f, 20, 20), "+x")) {
                def.lifeStages[currentLifestage].graphics.headOffset.x += 0.001f;
            }

            if (Widgets.ButtonText(new Rect(150, 330.0f, 20, 20), "++x")) {
                def.lifeStages[currentLifestage].graphics.headOffset.x += 0.025f;
            }

            if (Widgets.ButtonText(new Rect(20f, 360.0f, 20, 20), "--y")) {
                def.lifeStages[currentLifestage].graphics.headOffset.y -= 0.025f;
            }

            if (Widgets.ButtonText(new Rect(40f, 360.0f, 20, 20), "-y")) {
                def.lifeStages[currentLifestage].graphics.headOffset.y -= 0.001f;
            }

            Widgets.Label(new Rect(70f, 360.0f, 50, 22f), def.lifeStages[currentLifestage]?.graphics?.headOffset.y.ToString("0.000") ?? "NA");

            if (Widgets.ButtonText(new Rect(130f, 360.0f, 20, 20), "+y")) {
                def.lifeStages[currentLifestage].graphics.headOffset.y += 0.001f;
            }

            if (Widgets.ButtonText(new Rect(150f, 360.0f, 20, 20), "++y")) {
                def.lifeStages[currentLifestage].graphics.headOffset.y += 0.025f;
            }


            GUI.EndGroup();
        }

        /// <summary>
        /// Since we generate meshsets in the comp, we need to trick the comp into updating them with our testing values
        /// </summary>
        private void UpdateMeshsets() {
            foreach (Pawn pawn in Find.CurrentMap.mapPawns.AllPawnsSpawned.FindAll(p => p.TryGetComp<LifecycleComp>()?.LifecycleDef?.defName == currentDef)) {
                LifecycleComp comp = pawn.TryGetComp<LifecycleComp>();
                comp.GrowToStage(comp.CurrentLifestageIndex);
            }
        }

        private void DrawSelectors(Rect rect) {
            GUI.BeginGroup(rect);
            Color color = Widgets.NormalOptionColor;
            color.a = 50;
            if (Widgets.ButtonTextSubtle(new Rect(20f, 0.0f, 80, 22f), "Rot- " + currentDirection)) {
                Find.WindowStack.Add(new FloatMenu(directionsList));
            }

            if (Widgets.ButtonTextSubtle(new Rect(110, 0.0f, 80, 22f), "Stage- " + currentLifestage)) {
                Find.WindowStack.Add(new FloatMenu(lifestageIndexList));
            }

            if (Widgets.ButtonTextSubtle(new Rect(200, 0.0f, 80, 22f), "Part- " + selectedPart)) {
                Find.WindowStack.Add(new FloatMenu(partsList));
            }

            if (Widgets.ButtonTextSubtle(new Rect(290, 0.0f, 140, 22f),  currentDef)) {
                Find.WindowStack.Add(new FloatMenu(defsList));
            }

            if (Widgets.ButtonTextSubtle(new Rect(440, 0.0f, 22, 22f), "exp")) {
                RotationalAdjuster adj = def.lifeStages[currentLifestage]?.graphics?.bodyAddons?.Find(x => x.bodyPart == selectedPart)?.GetDirectionalAdjuster(currentDirection);
                CLog.Message("scale (" + adj?.scale.x + ", " + adj?.scale.y + ")");
                CLog.Message("offset (" + adj?.offset.x + ", " + adj?.offset.y + ")");
            }

            GUI.EndGroup();
        }

        protected override void SetInitialSizeAndPosition() {
            base.SetInitialSizeAndPosition();
            windowRect.x = storedX == 0.0f ? UI.screenWidth - windowRect.width : storedX;
            windowRect.y = storedY == 0.0f ? (UI.screenHeight - 35) - windowRect.height : storedY;
        }
    }
}
