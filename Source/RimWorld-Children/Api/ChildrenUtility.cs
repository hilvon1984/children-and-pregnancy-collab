﻿namespace RimWorldChildren.API {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using HarmonyLib;
    using RimWorld;
    using Verse;
    using Verse.AI;

    public static class ChildrenUtility {
        private static List<ThingDef> bedDefsBestToWorst_CribRestEffectiveness;

        /// <summary>
        /// A list of all bed defs ranked from best to worst prioritizing
        /// cribs over ordinary beds.
        /// </summary>
        public static List<ThingDef> AllBedDefBestToWorstCribRest {
            get {
                if (bedDefsBestToWorst_CribRestEffectiveness != null) {
                    return bedDefsBestToWorst_CribRestEffectiveness;
                } else {
                    bedDefsBestToWorst_CribRestEffectiveness = DefDatabase<ThingDef>.AllDefs.Where(def => def.IsBed).OrderByDescending(def => IsBedCrib(def)).ThenByDescending(d => d.GetStatValueAbstract(StatDefOf.BedRestEffectiveness, null)).ToList();
                    return bedDefsBestToWorst_CribRestEffectiveness;
                }
            }
        }

        /// <summary>
        /// As disabled worktypes are cached and loaded only once, we will do some gross stuff with reflection 
        /// here to forcefully clear the cache so that a pawn's disabled worktypes
        /// may be refreshed.
        /// </summary>
        /// <param name="pawn">The pawn to refresh</param>
        public static void RefreshDisabledWorkTypes(Pawn pawn) {
            Traverse.Create(pawn).Field("cachedDisabledWorkTypesPermanent").SetValue(null);
            Traverse.Create(pawn).Field("cachedDisabledWorkTypes").SetValue(null);
            foreach (SkillRecord skill in pawn.skills.skills) {
                Traverse.Create(skill).Field("cachedTotallyDisabled").SetValue(BoolUnknown.Unknown);
            }
        }

        /// <summary>
        /// This method will determine if a pawn is viable to be adopted into the colony.
        /// </summary>
        /// <param name="pawn">Maybe a baby pawn</param>
        /// <returns>True of the provided pawn could be adopted</returns>
        public static bool CanBeAdopted(Pawn pawn) {
            return ChildrenUtility.RaceUsesChildren(pawn) && ChildrenUtility.GetLifestageType(pawn) <= LifestageType.Toddler && (pawn.Faction != Faction.OfPlayer || pawn.IsPrisonerOfColony);
        }

        /// <summary>
        /// This method will roll for a chance to make the pawn Asexual, Bisexual, or Gay
        /// </summary>
        /// <param name="pawn">The pawn to be analyzed</param>
        /// <returns>An AcquirableTrait for sexuality</returns>
        public static AcquirableTrait GetSexuality(Pawn pawn) {
            // Variables for calculating likelihood of acquiring a sexuality trait are based upon statistics from Wikipedia
            // Link: https://en.wikipedia.org/wiki/Demographics_of_sexual_orientation
            // I am by no means an expert on sexuality, so if better statistics can be found, feel free to modify this code
            if (Rand.Value < 0.01) {
                if (Rand.Bool) {
                    return new AcquirableTrait(TraitDefOf.Asexual, 0, TraitDefOf.Asexual.GetGenderSpecificCommonality(pawn.gender));
                } else {
                    return new AcquirableTrait(TraitDefOf.Bisexual, 0, TraitDefOf.Bisexual.GetGenderSpecificCommonality(pawn.gender));
                }
            } else if (Rand.Value < 0.005) {
                return new AcquirableTrait(TraitDefOf.Gay, 0, TraitDefOf.Gay.GetGenderSpecificCommonality(pawn.gender));
            }

            return null;
        }

        public static bool ShouldBeFed(Pawn p) {
            if (!p.TryGetComp<LifecycleComp>()?.CurrentLifestage?.shouldBeFed ?? true) {
                return false;
            }

            if (p.GetPosture() == PawnPosture.Standing) {
                return false;
            }

            if (p.NonHumanlikeOrWildMan()) {
                Building_Bed building_Bed = p.CurrentBed();
                if (building_Bed == null || building_Bed.Faction != Faction.OfPlayer) {
                    return false;
                }
            } else {
                if (p.Faction != Faction.OfPlayer && p.HostFaction != Faction.OfPlayer) {
                    return false;
                }

                if (!p.InBed()) {
                    return false;
                }
            }

            if (!p.RaceProps.EatsFood) {
                return false;
            }

            if (p.HostFaction != null) {
                if (p.HostFaction != Faction.OfPlayer) {
                    return false;
                }

                if (p.guest != null && !p.guest.CanBeBroughtFood) {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// This method will return the LifestageType for a pawn's current lifestage,
        /// assuming that pawn has a LifeCycleComp and is properly initialized.
        /// Developers referencing this method are cautioned to consider whether its use
        /// makes a WIP feature inflexible. Should this feature instead be configurable in
        /// the LifestageDef?
        /// </summary>
        /// <param name="pawn">A pawn that may contain a LifeCycleComp</param>
        /// <returns>LifestageType of the pawn, or null if this pawn does not have a lifestage component</returns>
        public static LifestageType? GetLifestageType(Pawn pawn) {
            return pawn.TryGetComp<LifecycleComp>()?.CurrentLifestage?.lifestageType ?? null;
        }

        /// <summary>
        /// Returns true if a crying baby is nearby.
        /// </summary>
        /// <param name="pawn">pawn which looks for crying babies nearby</param>
        public static bool NearCryingBaby(Pawn pawn) {
            // Does not affect babies and toddlers
            if (ChildrenUtility.GetLifestageType(pawn) < LifestageType.Child || pawn.health.capacities.GetLevel(PawnCapacityDefOf.Hearing) <= 0.1f) {
                return false;
            }

            // Find any crying babies in the vicinity
            Room seekerRoom = pawn.PositionHeld.GetRoomOrAdjacent(pawn.MapHeld);
            foreach (Pawn mapPawn in pawn.MapHeld.mapPawns.AllPawnsSpawned) {
                if (mapPawn.health.hediffSet.HasHediff(HediffDef.Named("UnhappyBaby"))
                    && mapPawn.PositionHeld.InHorDistOf(pawn.PositionHeld, 24)
                    && mapPawn.PositionHeld.GetRoomOrAdjacent(mapPawn.MapHeld) == seekerRoom) {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Helper method to determine if a pawn ought to use a crib based on age/life stage
        /// </summary>
        /// <param name="pawn">maybe a baby?</param>
        /// <returns>True if the pawn should be using a crib</returns>
        public static bool ShouldUseCrib(Pawn pawn) {
            // Probably ought to change this based on size at some point, age stages are unreliable
            return pawn.TryGetComp<LifecycleComp>()?.CurrentLifestage?.usesCrib ?? false;
        }

        // Returns the maximum possible mass of a weapon the specified child can use
        public static float ChildMaxWeaponMass(Pawn pawn) {
            LifecycleComp comp = pawn.TryGetComp<LifecycleComp>();
            if (comp?.CurrentLifestage?.maxWeaponMass == null) {
                return 999;
            }

            return (pawn.skills.GetSkill(SkillDefOf.Shooting).Level * 0.1f) + (float)comp.CurrentLifestage.maxWeaponMass;
        }

        // Determines if a pawn is capable of currently breastfeeding
        public static bool CanBreastfeed(Pawn pawn) {
            return pawn.health.hediffSet.HasHediff(HediffDef.Named("Lactating"));
        }

        public static Building_Bed FindCribFor(Pawn baby, Pawn traveler) {
            Building_Bed crib = null;
            // Is a crib already assigned to the baby?
            if (baby.ownership?.OwnedBed != null && ChildrenUtility.IsBedCrib(baby.ownership.OwnedBed)) {
                Building_Bed bedThing = baby.ownership.OwnedBed;
                if (RestUtility.IsValidBedFor(bedThing, baby, traveler, false, false, false, true)) {
                    crib = baby.ownership.OwnedBed;
                }
            }
            // If not, let's look for one
            else {
                foreach (var thingDef in RestUtility.AllBedDefBestToWorst) {
                    if (RestUtility.CanUseBedEver(baby, thingDef) && thingDef.building.bed_maxBodySize <= 0.6f) {
                        Building_Bed find_crib = (Building_Bed)GenClosest.ClosestThingReachable(baby.Position, baby.Map, ThingRequest.ForDef(thingDef), PathEndMode.OnCell, TraverseParms.For(traveler), 9999f, (Thing b) => RestUtility.IsValidBedFor(b, baby, traveler, false, false), null);
                        if (find_crib != null) crib = find_crib;
                    }
                }
            }

            return crib;
        }

        public static bool IsBedCrib(Building_Bed bed) {
            return bed.def.building.bed_humanlike && bed.def.building.bed_maxBodySize <= 0.6f;
        }

        public static bool IsBedCrib(ThingDef bed) {
            return bed.building.bed_humanlike && bed.building.bed_maxBodySize <= 0.6f;
        }

        public static bool RaceUsesChildren(Pawn pawn) {
            return pawn != null && (ChildrenAndPregnancy.Settings?.RacialSettings(pawn.def.defName)?.lifeCycleEnabled ?? false);
        }

        public static bool RaceUsesChildren(string defName) {
            return ChildrenAndPregnancy.Settings?.RacialSettings(defName)?.lifeCycleEnabled ?? false;
        }

        /// <summary>
        /// Returns the accelerated growth factor setting value for a given growth stage.
        /// </summary>
        /// <returns>integer growth factor</returns>
        public static int SettingAcceleratedFactor(LifestageType growthStage) {
            switch (growthStage) {
                case LifestageType.Newborn:
                case LifestageType.Baby: return ChildrenAndPregnancy.Settings.babyAccelerationFactor - 1;
                case LifestageType.Toddler: return ChildrenAndPregnancy.Settings.toddlerAccelerationFactor - 1;
                case LifestageType.Child: return ChildrenAndPregnancy.Settings.childAccelerationFactor - 1;
            }

            return 1;
        }

        public static List<ThingStuffPair> FilterChildWeapons(Pawn pawn, List<ThingStuffPair> weapons) {
            var weapons_out = new List<ThingStuffPair>();
            if (weapons.Count > 0) {
                foreach (ThingStuffPair weapon in weapons) {
                    if (weapon.thing.BaseMass < ChildrenUtility.ChildMaxWeaponMass(pawn)) {
                        weapons_out.Add(weapon);
                    }
                }
            }

            return weapons_out;
        }

        /// <summary>
        /// Fetch a random body part matching the provided string
        /// </summary>
        /// <param name="pawn">The pawn whose parts will be searched</param>
        /// <param name="bodyPart">The string defName of the body part to be returned</param>
        /// <returns>A single bodypartrecord matching the provided string, or the only part if only one part exists</returns>
        public static BodyPartRecord GetPawnBodyPart(Pawn pawn, String bodyPart) {
            // Get collection of parts matching the def, then get a random left or right
            return pawn.RaceProps.body.AllParts.FindAll(x => x.def == DefDatabase<BodyPartDef>.GetNamed(bodyPart, true)).RandomElement();
        }

        /// <summary>
        /// Returns a collection of BodyPartRecords based on the part name provided.
        /// This may be a collection containing a single element, or multiple for left and right parts
        /// </summary>
        /// <param name="pawn">The pawn whose parts will be searched</param>
        /// <param name="bodyPart">The string defName of the body part to be returned</param>
        /// <returns>A collection of bodypart records</returns>
        public static List<BodyPartRecord> GetPawnBodyParts(Pawn pawn, String bodyPart) {
            return pawn.RaceProps.body.AllParts.FindAll(x => x.def == DefDatabase<BodyPartDef>.GetNamed(bodyPart, true));
        }

        /// <summary>
        /// This method will search for a capable pawn of the provided pawn's faction
        /// that will attempt to carry them off the map.
        /// </summary>
        /// <param name="pawn">The pawn to be carried off the map</param>
        /// <returns>true if a job was successfully assigned</returns>
        public static bool CarryFactionPawnToExit(Pawn pawn) {
            if (pawn.Downed) {
                foreach (Pawn carrier in pawn.Map.mapPawns.PawnsInFaction(pawn.Faction)) {
                    IntVec3 exitSpot;
                    if (carrier != pawn && !carrier.Downed && carrier.CurJobDef != JobDefOf.CarryDownedPawnToExit && RCellFinder.TryFindBestExitSpot(carrier, out exitSpot, TraverseMode.ByPawn)) {
                        Job job = JobMaker.MakeJob(JobDefOf.CarryDownedPawnToExit, pawn, exitSpot);
                        job.count = 1;
                        bool assigned = carrier.jobs.TryTakeOrderedJob(job, JobTag.Misc);
                        CLog.Message(carrier.Name + " will carry " + pawn.Name + " off map: " + assigned);
                        if (assigned) {
                            return assigned;
                        }
                    }
                }
            }

            return false;
        }
    }
}
