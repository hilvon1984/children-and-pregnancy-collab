﻿namespace RimWorldChildren {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using AlienRace;
    using HarmonyLib;
    using RimWorld;
    using RimWorldChildren.Api;
    using RimWorldChildren.API;
    using RimWorldChildren.Babygear;
    using UnityEngine;
    using Verse;
    using static AlienRace.AlienPartGenerator;

    [HarmonyPatch(typeof(PawnGraphicSet), "ResolveAllGraphics")]
    public static class PawnGraphicSet_ResolveAllGraphics_Patch {
        [HarmonyPostfix]
        internal static void ResolveAllGraphics_Patch(ref PawnGraphicSet __instance) {
            Pawn pawn = __instance.pawn;
            PawnGraphicSet _this = __instance;
            if (pawn.RaceProps.Humanlike) {
                Children_Drawing.ResolveAgeGraphics(__instance);
                LongEventHandler.ExecuteWhenFinished(delegate {
                    _this.ResolveApparelGraphics();
                });
            }
        }
    }

    [HarmonyPatch(typeof(PawnGraphicSet), "ResolveApparelGraphics")]
    public static class PawnGraphicSet_ResolveApparelGraphics_Patch {
        [HarmonyTranspiler]
        static IEnumerable<CodeInstruction> ResolveApparelGraphics_Transpiler(IEnumerable<CodeInstruction> instructions) {
            List<CodeInstruction> ILs = instructions.ToList();
            // from  if (ApparelGraphicRecordGetter.TryGetGraphicApparel(apparel, this.pawn.story.bodyType, out rec))
            // to  if (ApparelGraphicRecordGetter.TryGetGraphicApparel(apparel, ModifyChildBodyType(this.pawn), out rec))
            int injectIndex = ILs.FindIndex(ILs.FindIndex(x => x.opcode == OpCodes.Ldloca_S) + 1, x => x.opcode == OpCodes.Ldloca_S) - 2; // Second occurence
            MethodInfo childBodyCheck = typeof(Children_Drawing).GetMethod("ModifyChildBodyType");

            injectIndex = ILs.FindIndex(x => x.opcode == OpCodes.Ldloca_S) + 2; // after get_current for apparel enumerator
            MethodInfo apparelMockMethodInfo = typeof(Children_Drawing).GetMethod("InitializeApparelDatabase");
            List<CodeInstruction> apparelDbInject = new List<CodeInstruction> {
                new CodeInstruction(OpCodes.Ldarg_0),
                new CodeInstruction(OpCodes.Ldfld, typeof(PawnRenderer).GetField("pawn", AccessTools.all)),
                new CodeInstruction(OpCodes.Call, apparelMockMethodInfo),
            };
            ILs.InsertRange(injectIndex, apparelDbInject);

            foreach (CodeInstruction IL in ILs) {
                yield return IL;
            }
        }
    }


    public static class AlienRaces_GetNakedPath_Patch {
        /// <summary>
        /// Because HAR completely ignores the graphics paths contained in body types, and instead uses a
        /// path defined in the racial def, we need to perform an override.
        /// We are using body types that are not explicitly configured for the HAR def, so when one of these
        /// body types is assigned to a pawn, we will ignore HAR's preference for the racial def path
        /// and instead use the graphic path defined by the body type.
        /// </summary>
        public static bool GetNakedPath_Prefix(BodyTypeDef bodyType, ref string __result) {
            if (!ChildBodyTypeDefOf.IsOfType(bodyType)) {
                return true;
            }

            __result = bodyType.bodyNakedGraphicPath;
            return false;
        }
    }

    public static class AlienRaces_DrawAddons_Patch {
        public static Vector3 ModifyOffset(Vector3 vector, Pawn pawn, Rot4 rotation, object addon) {
            Vector3 newVector = vector;
            LifecycleComp comp = pawn.TryGetComp<LifecycleComp>();
            BodyAddon bodyAddon = (BodyAddon)addon;
            if (ChildrenUtility.RaceUsesChildren(pawn) && comp.CurrentLifestage?.graphics?.bodyAddons != null) {
                ScalableBodyAddon adjuster = comp.CurrentLifestage.graphics.bodyAddons.Find(ba => ba.bodyPart == bodyAddon.bodyPart);
                Vector2 extraOffset = Vector2.zero;
                if (adjuster == null) {
                    // If we cant find the specific part we're looking for, lets look for a wild card
                    adjuster = comp.CurrentLifestage.graphics?.bodyAddons?.Find(ba => ba.bodyPart == "*");
                }

                if (adjuster != null) {
                    BodyTypeDef referenceBody = (adjuster.referenceBody != null && !comp.CurrentLifestage.graphics.alternativeRendering) ?
                                                 adjuster.referenceBody : pawn.story.bodyType;
                    string referenceCrown = pawn.story.crownType.ToString();

                    // Since we cannot provide a custom offset for every part by default, let's try to make an educated guess
                    AlienPartGenerator.RotationOffset offset = rotation == Rot4.South ? bodyAddon.offsets.south :
                                                               rotation == Rot4.North ? bodyAddon.offsets.north :
                                                               rotation == Rot4.East ? bodyAddon.offsets.east :
                                                                                       bodyAddon.offsets.west;

                    if (referenceBody != null && offset.bodyTypes != null) {
                        Vector2 bodyOffset = offset.bodyTypes.Find(bod => bod.bodyType == referenceBody)?.offset ?? Vector2.zero;
                        extraOffset.x += bodyOffset.x;
                        extraOffset.y += bodyOffset.y;

                        // The IL for DrawAddons makes it nearly impossible to patch and replace the body type intelligently, so we just subtract the "wrong" offset
                        // This is necessary to use our reference body for body addon offsets
                        Vector2 originalOffset = offset.bodyTypes.Find(bod => bod.bodyType == pawn.story.bodyType)?.offset ?? Vector2.zero;
                        extraOffset.x -= originalOffset.x;
                        extraOffset.y -= originalOffset.y;
                    }

                    if (offset.crownTypes != null) {
                        Vector2 crownOffset = offset.crownTypes.Find(cr => cr.crownType == referenceCrown)?.offset ?? Vector2.zero;
                        extraOffset.x += crownOffset.x;
                        extraOffset.y += crownOffset.y;
                    }

                    if (rotation == Rot4.East) {
                        extraOffset.x = -extraOffset.x;
                    }

                    RotationalAdjuster offsets = adjuster.GetDirectionalAdjuster(rotation);
                    newVector.x += extraOffset.x;
                    newVector.z += extraOffset.y;
                    newVector.x *= offsets.offset.x;
                    newVector = CarryUtility.AdjustCarryLayer(newVector, pawn);

                    newVector.z *= offsets.offset.y;
                }
            }

            return newVector;
        }

        /// <summary>
        /// Adjust the scale of a Humanoid Alien Races body addon
        /// </summary>
        public static Graphic ModifyScale(Graphic graphic, Pawn pawn, Rot4 rotation, object addon) {
            Graphic newGraphic = graphic;
            LifecycleComp comp = pawn.TryGetComp<LifecycleComp>();
            BodyAddon bodyAddon = (BodyAddon)addon;
            if (ChildrenUtility.RaceUsesChildren(pawn) && comp.CurrentLifestage?.graphics?.bodyAddons != null) {
                ScalableBodyAddon adjuster = comp.CurrentLifestage.graphics.bodyAddons.Find(ba => ba.bodyPart == bodyAddon.bodyPart);
                if (adjuster == null) {
                    // If we cant find the specific part we're looking for, lets look for a wild card
                    adjuster = comp.CurrentLifestage.graphics.bodyAddons.Find(ba => ba.bodyPart == "*");
                }

                if (adjuster != null) {
                    RotationalAdjuster offsets = adjuster.GetDirectionalAdjuster(rotation);
                    Vector2 scale = offsets.scale;
                    scale.x *= graphic.drawSize.x;
                    scale.y *= graphic.drawSize.y;
                    newGraphic = graphic.GetCopy(scale);
                    newGraphic.drawSize = scale;
                }
            }

            return newGraphic;
        }

        /// <summary>
        /// Patch Humanoid Alien Races' DrawAddons method. Here we add our own offsets and scale adjustments
        /// based on lifestage def criteria. Some of this logic could probably be handled with an XML patch,
        /// however it is all handled here to keep the settings colocated.
        /// This would be infinitely simplified if HAR provided a mechanism for other mods to provide additional
        /// offset or scale adjustments through an API.
        /// </summary>
        static IEnumerable<CodeInstruction> DrawAddons_Transpiler(IEnumerable<CodeInstruction> instructions) {
            List<CodeInstruction> ils = instructions.ToList();
            MethodInfo meshAtInfo = AccessTools.Method(typeof(Graphic), "MeshAt");

            for (int i = 0; i < ils.Count; ++i) {
                if (i > 2 && ils[i - 2].opcode == OpCodes.Ldarg_2) { // offsetVector
                    // from vector + offsetVector.RotatedBy...
                    // to vector + ModifyOffset(offsetVector, pawn, rotation, bodyAddon).RotatedBy...
                    yield return ils[i];
                    yield return new CodeInstruction(OpCodes.Ldarg_3); // pawn
                    yield return new CodeInstruction(OpCodes.Ldarg_S, 5); // rotation
                    yield return new CodeInstruction(OpCodes.Ldloc_S, 4); // bodyAddon
                    yield return new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(AlienRaces_DrawAddons_Patch), "ModifyOffset"));
                } else if (ils[i].opcode == OpCodes.Ldloc_S && ils[i + 2].OperandIs(meshAtInfo) && ils[i + 1].opcode == OpCodes.Ldarg_S && ils[i + 1].OperandIs(5)) { // local 14 = addonGraphic
                    // from addonGraphic.MeshAt(rotation)
                    // to ModifyScale(addonGraphic, pawn, bodyAddon).MeshAt(rot: rotation)
                    yield return ils[i];
                    yield return new CodeInstruction(OpCodes.Ldarg_3); // pawn
                    yield return new CodeInstruction(OpCodes.Ldarg_S, 5); // rotation
                    yield return new CodeInstruction(OpCodes.Ldloc_S, 4); // bodyAddon
                    yield return new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(AlienRaces_DrawAddons_Patch), "ModifyScale"));
                } else {
                    yield return ils[i];
                }
            }
        }
    }

    public static class AlienRaces_BodyAddon_Patch {
        /// <summary>
        /// Check prior to execution in order to hide addons
        /// </summary>
        internal static bool CanDrawAddon_Prefix(Pawn pawn, ref bool __result, object __instance) {
            LifecycleComp comp = pawn.TryGetComp<LifecycleComp>();
            if (ChildrenUtility.RaceUsesChildren(pawn) && ((!comp?.CurrentLifestage?.graphics?.bodyAddons?.Find(ba => ba.bodyPart == ((BodyAddon)__instance).bodyPart)?.renderPart ?? false)
                                                            || (!comp?.CurrentLifestage?.graphics?.bodyAddons?.Find(ba => ba.bodyPart == "*")?.renderPart ?? false))) {
                __result = false;
                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(PawnRenderer), "RenderPawnInternal", new[] { typeof(Vector3), typeof(float), typeof(Boolean), typeof(Rot4), typeof(Rot4), typeof(RotDrawMode), typeof(Boolean), typeof(Boolean), typeof(Boolean) })]
    [HarmonyBefore(new string[] { "rimworld.erdelf.alien_race.main" })]
    public static class PawnRenderer_RenderPawnInternal_Patch {
        /// <summary>
        /// Adjust Y position for child pawns at the start of the method so we don't have to do
        /// transpiler patching.
        /// </summary>
        [HarmonyPrefix]
        internal static void RenderPawnInternal_Patch(ref PawnGraphicSet __instance, ref Vector3 rootLoc, Pawn ___pawn, bool portrait) {
            // Change the root location of the child's draw position
            rootLoc = Children_Drawing.ModifyChildYPosOffset(rootLoc, ___pawn, portrait);
        }

        public static Mesh GetHeadMesh(Pawn pawn, Rot4 headFacing) {
            if (pawn.TryGetComp<LifecycleComp>()?.CurrentLifestage?.graphics != null) {
                return pawn.TryGetComp<LifecycleComp>().Meshsets.headMeshSet.MeshAt(headFacing);
            }

            return MeshPool.humanlikeHeadSet.MeshAt(headFacing);
        }

        public static Mesh GetBodyMesh(Pawn pawn, Rot4 bodyFacing) {
            if (pawn.TryGetComp<LifecycleComp>()?.CurrentLifestage?.graphics != null ) {
                return pawn.TryGetComp<LifecycleComp>().Meshsets.bodyMeshSet.MeshAt(bodyFacing);
            }

            return MeshPool.humanlikeBodySet.MeshAt(bodyFacing);
        }

        /// <summary>
        /// Gets a resized mesh for hair based on equipped apparel and other info.
        /// </summary>
        /// <returns>An unaltered hair mesh if the pawn has baby gear equipped in the Overhead layer.
        ///          Otherwise, a resized hair mesh as defined in the Lifecycle def.</returns>
        public static Mesh GetHairMesh(PawnGraphicSet graphics, Pawn pawn, Rot4 headFacing) {
            if (pawn.TryGetComp<LifecycleComp>()?.CurrentLifestage?.graphics != null && !pawn.apparel.WornApparel.Any(a => a.TryGetComp<CompBabyGear>() != null && a.def.apparel.LastLayer == ApparelLayerDefOf.Overhead)) {
                return pawn.TryGetComp<LifecycleComp>().Meshsets.hairMeshSet.MeshAt(headFacing);
            }

            return graphics.HairMeshSet.MeshAt(headFacing);
        }

        [HarmonyTranspiler]
        static IEnumerable<CodeInstruction> RenderPawnInternal_Transpiler(IEnumerable<CodeInstruction> instructions, ILGenerator ILgen) {
            List<CodeInstruction> ILs = instructions.ToList();

            FieldInfo humanlikeBodyInfo = AccessTools.Field(type: typeof(MeshPool), name: nameof(MeshPool.humanlikeBodySet));
            FieldInfo humanlikeHeadInfo = AccessTools.Field(type: typeof(MeshPool), name: nameof(MeshPool.humanlikeHeadSet));
            FieldInfo apparelGraphicsInfo = typeof(PawnRenderer).GetField("graphics", AccessTools.all).GetType().GetField("apparelGraphics", AccessTools.all);
            MethodInfo hairMeshSetInfo = AccessTools.Property(type: typeof(PawnGraphicSet), name: nameof(PawnGraphicSet.HairMeshSet)).GetGetMethod();
            MethodInfo hairMatInfo = AccessTools.Method(type: typeof(PawnGraphicSet), name: nameof(PawnGraphicSet.HairMatAt_NewTemp));
            MethodInfo drawMeshInfo = AccessTools.Method(type: typeof(GenDraw), name: nameof(GenDraw.DrawMeshNowOrLater));
            MethodInfo overrideMaterialIfNeededInfo = AccessTools.Method(typeof(PawnRenderer), "OverrideMaterialIfNeeded_NewTemp", new[] { typeof(Material), typeof(Pawn), typeof(bool) });
            // from GenDraw.DrawMeshNowOrLater(MeshPool.humanlikeHeadSet.MeshAt(headFacing), vector3_2 + vector3_3, quaternion, mat1, portrait);
            // to GenDraw.DrawMeshNowOrLater(GetHeadMesh(pawn, headFacing), vector3_2 + vector3_3, quaternion, mat1, portrait);
            int humanlikeHeadIndex = ILs.FindIndex(x => x.OperandIs(humanlikeHeadInfo));
            List<CodeInstruction> meshInject = new List<CodeInstruction> {
                new CodeInstruction(OpCodes.Ldarg_0),
                new CodeInstruction(OpCodes.Ldfld, typeof(PawnRenderer).GetField("pawn", AccessTools.all)),
                new CodeInstruction(OpCodes.Ldarg_S, 5), // headFacing
                new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(PawnRenderer_RenderPawnInternal_Patch), nameof(PawnRenderer_RenderPawnInternal_Patch.GetHeadMesh))),
            };
            ILs.RemoveRange(humanlikeHeadIndex, 3);
            ILs.InsertRange(humanlikeHeadIndex, meshInject);

            // from GenDraw.DrawMeshNowOrLater(GetHeadMesh(pawn, headFacing), vector3_2 + vector3_3, quaternion, mat1, portrait);
            // to GenDraw.DrawMeshNowOrLater(GetHeadMesh(pawn, headFacing), vector3_2 + HeadOffsetAdjustment(vector3_3), quaternion, mat1, portrait);
            int afterVectorAddIndex = ILs.FindIndex(x => x.opcode == OpCodes.Ldloc_0);
            meshInject = new List<CodeInstruction> {
                 new CodeInstruction(OpCodes.Ldarg_0),
                 new CodeInstruction(OpCodes.Ldfld, typeof(PawnRenderer).GetField("pawn", AccessTools.all)),
                 new CodeInstruction(OpCodes.Ldarg_S, 5), // headFacing
                 new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(Children_Drawing), nameof(Children_Drawing.HeadOffsetAdjustment))),
            };
            ILs.InsertRange(humanlikeHeadIndex + 6, meshInject); // +6 here because we remove 3, add 4 above

            // from MeshPool.humanlikeBodySet.MeshAt(bodyFacing);
            // to GetBodyMesh(pawn, bodyFacing)
            int humanlikeBodyIndex = ILs.FindIndex(x => x.OperandIs(humanlikeBodyInfo));
            meshInject = new List<CodeInstruction> {
                new CodeInstruction(OpCodes.Ldarg_0),
                new CodeInstruction(OpCodes.Ldfld, typeof(PawnRenderer).GetField("pawn", AccessTools.all)),
                new CodeInstruction(OpCodes.Ldarg_S, 4), // bodyFacing
                new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(PawnRenderer_RenderPawnInternal_Patch), nameof(PawnRenderer_RenderPawnInternal_Patch.GetBodyMesh))),
            };
            ILs.RemoveRange(humanlikeBodyIndex, 3);
            ILs.InsertRange(humanlikeBodyIndex, meshInject);

            // from Mesh mesh2 = this.graphics.HairMeshSet.MeshAt(headFacing);
            // to  Mesh mesh2 = GetHairMesh(this.graphics, pawn, rotation);
            List<int> hairMeshSetInstructions = Enumerable.Range(0, ILs.Count).Where(x => ILs[x].OperandIs(hairMeshSetInfo)).ToList();
            meshInject = new List<CodeInstruction> {
                new CodeInstruction(OpCodes.Ldarg_0),
                new CodeInstruction(OpCodes.Ldfld, typeof(PawnRenderer).GetField("pawn", AccessTools.all)),
                new CodeInstruction(OpCodes.Ldarg_S, 5), // headFacing
                new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(PawnRenderer_RenderPawnInternal_Patch), nameof(PawnRenderer_RenderPawnInternal_Patch.GetHairMesh))),
            };

            for (int i = 0; i < hairMeshSetInstructions.Count; ++i) {
                // Adjust index since we are injecting to multiple places
                int index = hairMeshSetInstructions[i] + (i * (meshInject.Count - 3));
                ILs.RemoveRange(index, 3);
                ILs.InsertRange(index, meshInject);
            }

            List<int> drawMeshInfoInstructions = Enumerable.Range(0, ILs.Count).Where(x => ILs[x].OperandIs(drawMeshInfo)).ToList();

            // From GenDraw.DrawMeshNowOrLater(mesh2, loc2, quat, mat2, num != 0);
            // To GenDraw.DrawMeshNowOrLater(mesh2, CarryUtility.AdjustCarryLayer(loc2, pawn), quat, mat2, num != 0);
            List<CodeInstruction> hairCarryLayerInject = new List<CodeInstruction> {
                new CodeInstruction(OpCodes.Ldarg_0),
                new CodeInstruction(OpCodes.Ldfld, typeof(PawnRenderer).GetField("pawn", AccessTools.all)),
                new CodeInstruction(OpCodes.Call, typeof(CarryUtility).GetMethod("AdjustCarryLayer")),
            };
            ILs.InsertRange(drawMeshInfoInstructions[4] - 3, hairCarryLayerInject);

            // Render body for babies and toddlers
            // from if (renderBody) to if (Children_Utility.GetLifestageType(this.pawn) < LifestageType.Child || renderBody)
            int renderBodyIndex = ILs.FindIndex(x => x.opcode == OpCodes.Ldarg_3);
            Label babyDrawBodyJump = ILgen.DefineLabel();
            ILs[renderBodyIndex + 2].labels = new List<Label> { babyDrawBodyJump }; // Label entrance to the code block
            List<CodeInstruction> injection1 = new List<CodeInstruction> {
                new CodeInstruction(OpCodes.Ldarg_0),
                new CodeInstruction(OpCodes.Ldfld, typeof(PawnRenderer).GetField("pawn", AccessTools.all)),
                new CodeInstruction(OpCodes.Call, typeof(Children_Drawing).GetMethod("IsChildOrOlder")),
                new CodeInstruction(OpCodes.Brfalse, babyDrawBodyJump), // Jump renderBody and enter block
            };
            ILs.InsertRange(renderBodyIndex, injection1);

            // Ensure pawn is a child or higher before drawing head
            // from if (this.graphics.headGraphic != null)
            // to if (this.graphics.headGraphic != null || Children_Drawing.ShouldRenderHead(pawn))
            int injectIndex2 = ILs.FindIndex(x => x.opcode == OpCodes.Ldfld && x.operand == AccessTools.Field(typeof(PawnGraphicSet), "headGraphic")) + 2;
            List<CodeInstruction> injection2 = new List<CodeInstruction> {
                new CodeInstruction(OpCodes.Ldarg_0),
                new CodeInstruction(OpCodes.Ldfld, typeof(PawnRenderer).GetField("pawn", AccessTools.all)),
                new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(Children_Drawing), nameof(Children_Drawing.ShouldRenderHead))),
                new CodeInstruction(OpCodes.Brfalse, ILs[injectIndex2 - 1].operand),
            };
            ILs.InsertRange(injectIndex2, injection2);

            // Modify the scale of a hair graphic when drawn on a child
            // from Material material = this.graphics.HairMatAt_NewTemp(headFacing, portrait);
            // to Material material = ModifyHairForChild(this.graphics.HairMatAt_NewTemp(headFacing, portrait));
            int injectIndex4 = ILs.FindIndex(x => x.opcode == OpCodes.Callvirt && x.operand == hairMatInfo) + 1;

            List<CodeInstruction> injection4 = new List<CodeInstruction> {
                new CodeInstruction (OpCodes.Ldarg_0),
                new CodeInstruction (OpCodes.Ldfld, typeof(PawnRenderer).GetField("pawn", AccessTools.all)),
                new CodeInstruction (OpCodes.Call, AccessTools.Method(typeof(Children_Drawing), "ModifyHairForChild")),
            };
            ILs.InsertRange(injectIndex4, injection4);

            // Modify the scale of clothing graphics when worn by a child
            List<int> overrideMaterialInstructions = Enumerable.Range(0, ILs.Count).Where(x => ILs[x].opcode == OpCodes.Call && ILs[x].operand == overrideMaterialIfNeededInfo).ToList();

            List<CodeInstruction> modifyHatForChildInjection = new List<CodeInstruction> {
                new CodeInstruction(OpCodes.Ldarg_0),
                new CodeInstruction(OpCodes.Ldfld, typeof(PawnRenderer).GetField("pawn", AccessTools.all)),
                new CodeInstruction(OpCodes.Ldarg_S, 4), // bodyFacing
                new CodeInstruction(OpCodes.Call, typeof(Children_Drawing).GetMethod("ModifyHatForChild")),
            };

            // Modify the first and fourth calls for the body + lower layers, and shell layers
            // Modify the second and third calls for hats
            ILs.InsertRange(overrideMaterialInstructions[1] + 1, modifyHatForChildInjection);
            ILs.InsertRange(overrideMaterialInstructions[2] + 1 + modifyHatForChildInjection.Count, modifyHatForChildInjection);

            foreach (CodeInstruction IL in ILs) {
                yield return IL;
            }
        }
    }

    internal static class Children_Drawing {

        /// <summary>
        /// For ease of patching, this method will dump the input Apparel instance back onto the stack.
        /// Attempt to pre-load the graphics database with body overrides.
        /// </summary>
        /// <returns>The input Apparel instance, unmodified</returns>
        public static Apparel InitializeApparelDatabase(Apparel apparel, Pawn pawn) {
            LifecycleComp comp = pawn.TryGetComp<LifecycleComp>();
            if (ChildrenUtility.RaceUsesChildren(pawn)) {
                // Skip if the apparel isn't one we resize in the first place
                GraphicsPreloader.PreloadApparelGraphic(apparel, pawn);
            }

            return apparel;
        }

        /// <summary>
        /// Determines if a pawn's head should be rendered if they are configured to use life stage rendering
        /// </summary>
        public static bool ShouldRenderHead(Pawn pawn) {
            LifecycleComp comp = pawn.TryGetComp<LifecycleComp>();
            if (ChildrenUtility.RaceUsesChildren(pawn) && comp?.CurrentLifestage?.graphics != null) {
                return comp.CurrentLifestage.graphics.renderHead;
            }

            return true;
        }

        public static Vector3 HeadOffsetAdjustment(Vector3 vector, Pawn pawn, Rot4 headFacing) {
            LifecycleComp comp = pawn.TryGetComp<LifecycleComp>();
            if (ChildrenUtility.RaceUsesChildren(pawn) && comp?.CurrentLifestage?.graphics != null) {
                Vector2 offset = comp.CurrentLifestage.graphics.headOffset;
                vector.x *= offset.x;
                vector.z *= offset.y;
            }

            return vector;
        }

        internal static void ResolveAgeGraphics(PawnGraphicSet graphics) {
            LongEventHandler.ExecuteWhenFinished(delegate {
                LifecycleComp comp = graphics.pawn.TryGetComp<LifecycleComp>();
                if (!ChildrenUtility.RaceUsesChildren(graphics.pawn) || comp?.CurrentLifestage?.graphics == null
                    || comp.CurrentLifestage.graphics.alternativeRendering) {
                    return;
                }

                if (graphics.pawn.story.hairDef != null) {
                    graphics.hairGraphic = GraphicDatabase.Get<Graphic_Multi>(graphics.pawn.story.hairDef.texPath, ShaderDatabase.Cutout, Vector2.one, graphics.pawn.story.hairColor);
                }

                if (!comp.CurrentLifestage.graphics.headGraphicPath.NullOrEmpty()) {
                    graphics.headGraphic = GraphicDatabase.Get<Graphic_Multi>(comp.CurrentLifestage.graphics.headGraphicPath, ShaderDatabase.CutoutSkin, comp.CurrentLifestage.graphics.headScale, graphics.pawn.story.SkinColor) as Graphic_Multi;
                }

                if (!comp.CurrentLifestage.graphics.bodyGraphicsPath.NullOrEmpty()) {
                    graphics.nakedGraphic = GraphicDatabase.Get<Graphic_Multi>(comp.CurrentLifestage.graphics.bodyGraphicsPath, ShaderDatabase.CutoutSkin, comp.CurrentLifestage.graphics.bodyScale, graphics.pawn.story.SkinColor) as Graphic_Multi;
                    graphics.rottingGraphic = GraphicDatabase.Get<Graphic_Multi>(comp.CurrentLifestage.graphics.bodyGraphicsPath, ShaderDatabase.CutoutSkin, comp.CurrentLifestage.graphics.bodyScale, PawnGraphicSet.RottingColor) as Graphic_Multi;
                }

                if (!comp.CurrentLifestage.graphics.hairGraphicPath.NullOrEmpty()) {
                    string gender = comp.CurrentLifestage.graphics.genderedHair ? "_" + graphics.pawn.gender.ToString() : string.Empty;
                    graphics.hairGraphic = GraphicDatabase.Get<Graphic_Multi>(comp.CurrentLifestage.graphics.hairGraphicPath + gender, ShaderDatabase.Cutout, Vector2.one, graphics.pawn.story.hairColor);
                }
            });
        }

        /// <summary>
        /// This patch will use a different body type than the one the pawn has assigned to
        /// handle clothes rendering. This may be done in cases where a bodytype has no clothes graphics
        /// of its own.
        /// </summary>
        public static BodyTypeDef ModifyChildBodyType(Pawn pawn) {
            LifecycleComp comp = pawn.TryGetComp<LifecycleComp>();
            if (ChildrenUtility.RaceUsesChildren(pawn) && comp?.CurrentLifestage?.graphics?.clothesBodyType != null && !comp.CurrentLifestage.graphics.alternativeRendering) {
                return comp.CurrentLifestage.graphics.clothesBodyType;
            }

            return pawn.story.bodyType;
        }

        public static Vector3 ModifyChildYPosOffset(Vector3 pos, Pawn pawn, bool portrait) {
            Vector3 newPos = pos;
            if (ChildrenUtility.RaceUsesChildren(pawn)) {
                // move the draw target down to compensate for child shortness
                if (ChildrenUtility.GetLifestageType(pawn) == LifestageType.Child && !pawn.InBed()) {
                    newPos.z -= 0.15f;
                }
                if (pawn.InBed() && !portrait && pawn.CurrentBed().def.size.z == 1) {
                    Building_Bed bed = pawn.CurrentBed();
                    // Babies and toddlers get drawn further down along the bed
                    if (ChildrenUtility.GetLifestageType(pawn) < LifestageType.Child) {
                        Vector3 vector = new Vector3(0, 0, 0.5f).RotatedBy(bed.Rotation.AsAngle);
                        newPos -= vector;
                        // ... as do children, but to a lesser extent
                    }
                    else if (ChildrenUtility.GetLifestageType(pawn) == LifestageType.Child) { // Are we in a crib?
                        Vector3 vector = new Vector3(0, 0, 0.2f).RotatedBy(bed.Rotation.AsAngle);
                        newPos -= vector;
                    }

                    newPos.z += 0.2f;
                }
            }

            return newPos;
        }

        public static Material ModifyHatForChild(Material mat, Pawn pawn, Rot4 bodyFacing) {
            if (mat == null) {
                return null;
            }

            Material newMat = mat;
            if (ChildrenUtility.RaceUsesChildren(pawn) && pawn.TryGetComp<LifecycleComp>()?.CurrentLifestage?.graphics != null) {
                newMat.mainTexture.wrapMode = TextureWrapMode.Clamp;
                newMat.mainTextureOffset = pawn.TryGetComp<LifecycleComp>().CurrentLifestage.graphics.hatOffsetNS;
                if (bodyFacing == Rot4.West || bodyFacing == Rot4.East) {
                    newMat.mainTextureOffset = pawn.TryGetComp<LifecycleComp>().CurrentLifestage.graphics.hatOffsetEW;
                }
            }

            return newMat;
        }

        public static bool IsChildOrOlder(Pawn pawn) {
            return ChildrenUtility.GetLifestageType(pawn) >= LifestageType.Child;
        }

        /// <summary>
        /// Adjust the scale and offset of hair when dictated by the LifecycleComp
        /// </summary>
        /// <param name="mat">Hiar material to be adjusted</param>
        /// <param name="pawn">The pawn who is getting a new doo</param>
        /// <returns>Adjusted material, or unmodified if scaling is off</returns>
        public static Material ModifyHairForChild(Material mat, Pawn pawn) {
            Material newMat = mat;
            LifecycleComp comp = pawn.TryGetComp<LifecycleComp>();
            if (ChildrenUtility.RaceUsesChildren(pawn) && pawn.TryGetComp<LifecycleComp>()?.CurrentLifestage?.graphics != null) {
                newMat.mainTexture.wrapMode = TextureWrapMode.Clamp;

                Vector2 hairOffset = comp.CurrentLifestage.graphics.hairOffset;
                newMat.mainTextureOffset = hairOffset;
            }

            return newMat;
        }
    }
}