﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using HarmonyLib;
    using RimWorld;
    using RimWorldChildren.API;
    using RimWorldChildren.Babygear;
    using UnityEngine;
    using Verse;
    using Verse.AI;

    /// <summary>
    /// Harmony patches for classes related to apparel, clothing, and jobdrivers
    /// </summary>
    public class OptimizeApparelPatches {
        [HarmonyPatch(typeof(JobGiver_OptimizeApparel), "TryGiveJob")]
        public static class JobDriver_OptimizeApparel_Patch {
            [HarmonyPostfix]
            public static void TryGiveJob_Patch(JobGiver_OptimizeApparel __instance, ref Job __result, Pawn pawn) {
                if (__result != null) {
                    // Stop the game from automatically allocating pawns Wear jobs they cannot fulfil
                    if ((ChildrenUtility.GetLifestageType(pawn) <= LifestageType.Toddler && __result.targetA.Thing.TryGetComp<CompBabyGear>() == null && ChildrenUtility.RaceUsesChildren(pawn))
                            || (ChildrenUtility.GetLifestageType(pawn) >= LifestageType.Child && __result.targetA.Thing.TryGetComp<CompBabyGear>() != null)) {
                        __result = null;
                    }
                }
                else {
                    // makes pawn to remove baby clothes when too old for them.
                    List<Apparel> wornApparel = pawn.apparel.WornApparel;
                    if (ChildrenUtility.GetLifestageType(pawn) >= LifestageType.Child) {
                        for (int i = wornApparel.Count - 1; i >= 0; i--) {
                            CompBabyGear compBabyGear = wornApparel[i].TryGetComp<CompBabyGear>();
                            if (compBabyGear != null) {
                                __result = new Job(JobDefOf.RemoveApparel, wornApparel[i]) {
                                    haulDroppedApparel = true,
                                };
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Used to force unequip apparel on age up
    /// </summary>
    [HarmonyPatch(typeof(Pawn_AgeTracker), "BirthdayBiological")]
    public static class AgeTracker_BirthdayBiological_Patch {
            public static void Postfix(Pawn_AgeTracker __instance) {
                Traverse traverser = Traverse.Create(__instance);
                Pawn pawn = traverser.Field("pawn").GetValue<Pawn>();
                LifecycleComp comp = pawn?.GetComp<LifecycleComp>();

                if (comp != null) {
                    comp.ForceUpdate(true);
                    List<Apparel> wornApparel = pawn.apparel?.WornApparel;
                    if (wornApparel != null) {
                        for (int i = 0; i < wornApparel.Count; ++i) {
                            CompBabyGear gearComp = wornApparel[i].TryGetComp<CompBabyGear>();

                            // Need to revise this to be more intelligent
                            if (gearComp != null && gearComp.Props.isBabyGear && comp.CurrentLifestage.lifestageType > LifestageType.Toddler) {
                                pawn.apparel.TryDrop(wornApparel[i]);
                            }
                        }
                    }

                    if (pawn.mindState != null) {
                        pawn.mindState.nextApparelOptimizeTick = Find.TickManager.TicksGame;
                    }
                }
            }
    }

    // Prevents babies from wearing normal clothes
    // Needs to be expanded upon when toddler clothing is added
    [HarmonyPatch(typeof(JobDriver_Wear), "MakeNewToils")]
    internal static class Wear_Override {
        [HarmonyPrefix]
        internal static void JobDriver_Wear_Patch(ref JobDriver_Wear __instance) {
            Pawn actor = __instance.GetActor();
            Job job = __instance.job;
            __instance.FailOn(delegate {
                // TODO: Revisit to handle different types of gear comps per race
                if (ChildrenUtility.RaceUsesChildren(actor) && actor.TryGetComp<LifecycleComp>()?.CurrentLifestage.lifestageType <= LifestageType.Toddler && job.GetTarget(TargetIndex.A).Thing.TryGetComp<CompBabyGear>() == null
                    && actor.TryGetComp<LifecycleComp>()?.CurrentLifestage?.graphics != null && !actor.TryGetComp<LifecycleComp>().CurrentLifestage.graphics.alternativeRendering) {
                    Messages.Message("MessageAdultClothesOnBaby".Translate(actor.Name.ToStringShort), actor, MessageTypeDefOf.CautionInput);
                    return true;
                }

                // Disable baby gear for babies when their def uses alternative rendering - Graphics will almost certainly never exist for the body type in this case
                // tried testing for bool "isBabyGear" but that caused a null error exception, so I switched to just testing if comp was null, will look into later
                if ((actor.TryGetComp<LifecycleComp>()?.CurrentLifestage.lifestageType > LifestageType.Toddler && job.GetTarget(TargetIndex.A).Thing.TryGetComp<CompBabyGear>() != null)
                    || (job.GetTarget(TargetIndex.A).Thing.TryGetComp<CompBabyGear>() != null && actor.TryGetComp<LifecycleComp>()?.CurrentLifestage?.graphics != null && actor.TryGetComp<LifecycleComp>().CurrentLifestage.graphics.alternativeRendering)) {
                    Messages.Message("MessageBabyClothesOnAdult".Translate(actor.LabelShort), actor, MessageTypeDefOf.CautionInput);
                    return true;
                }

                return false;
            });
        }
    }

    /// <summary>
    /// Apparel graphic patch to ensure toddler clothing uses bodytype graphics even for hats.
    /// </summary>
    [HarmonyPatch(typeof(ApparelGraphicRecordGetter), "TryGetGraphicApparel")]
    public static class ApparelGraphicRecordGetter_TryGetGraphicApparel_Patch {

        /// <summary>
        /// From string path = apparel.def.apparel.LastLayer == ApparelLayerDefOf.Overhead || apparel.def.apparel.wornGraphicPath == BaseContent.PlaceholderImagePath ? apparel.def.apparel.wornGraphicPath : apparel.def.apparel.wornGraphicPath + "_" + bodyType.defName;
        /// To string path = !ToddlerOverride(apparel) || (apparel.def.apparel.LastLayer == ApparelLayerDefOf.Overhead || apparel.def.apparel.wornGraphicPath == BaseContent.PlaceholderImagePath) ? apparel.def.apparel.wornGraphicPath : apparel.def.apparel.wornGraphicPath + "_" + bodyType.defName;
        /// </summary>
        [HarmonyTranspiler]
        public static IEnumerable<CodeInstruction> TryGetGraphicApparel_Transpiler(IEnumerable<CodeInstruction> instructions, ILGenerator ILgen) {
            List<CodeInstruction> instructionList = instructions.ToList();

            MethodInfo lastLayerGetInfo = AccessTools.PropertyGetter(type: typeof(ApparelProperties), name: nameof(ApparelProperties.LastLayer));
            int ternaryFalseIndex = instructionList.FindIndex(c => c.opcode == OpCodes.Ldstr && c.OperandIs("_")) - 4; // ldarg.0 - apparel, the "false" part of the ternary
            Label ternaryJump = ILgen.DefineLabel();
            instructionList[ternaryFalseIndex].labels.Add(ternaryJump);

            int insertIndex = instructionList.FindIndex(c => c.opcode == OpCodes.Callvirt && c.OperandIs(lastLayerGetInfo)) - 3; // ldarg.0 - apparel, the first instruction of the ternary

            List<CodeInstruction> injection = new List<CodeInstruction> {
                new CodeInstruction(OpCodes.Ldarg_0),
                new CodeInstruction(OpCodes.Call, typeof(ApparelGraphicRecordGetter_TryGetGraphicApparel_Patch).GetMethod(nameof(ApparelGraphicRecordGetter_TryGetGraphicApparel_Patch.ToddlerOverride))),
                new CodeInstruction(OpCodes.Brtrue, ternaryJump),
            };
            injection[0].labels = new List<Label>(instructionList[insertIndex].labels); // Move the labels to the new "start" of the ternary
            instructionList[insertIndex].labels = null;
            instructionList.InsertRange(insertIndex, injection);
            foreach (CodeInstruction il in instructionList) {
                yield return il;
            }
        }

        /// <summary>
        /// Checks the apparel for the BabyGear component.
        /// </summary>
        /// <returns>True if the apparel has the Baby Gear Component</returns>
        public static bool ToddlerOverride(Apparel apparel) {
            return apparel.TryGetComp<CompBabyGear>() != null;
        }
    }
}