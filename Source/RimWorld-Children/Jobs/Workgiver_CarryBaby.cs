﻿namespace RimWorldChildren {
    using RimWorld;
    using RimWorldChildren.Api;
    using RimWorldChildren.API;
    using System.Collections.Generic;
    using Verse;
    using Verse.AI;

    public class Workgiver_CarryBaby : WorkGiver_Scanner {
        public override ThingRequest PotentialWorkThingRequest {
            get {
                return ThingRequest.ForGroup(ThingRequestGroup.Pawn);
            }
        }

        public override PathEndMode PathEndMode {
            get {
                return PathEndMode.ClosestTouch;
            }
        }

        public override Danger MaxPathDanger(Pawn pawn) {
            return Danger.Deadly;
        }

        public override IEnumerable<Thing> PotentialWorkThingsGlobal(Pawn pawn) {
            return pawn.Map.mapPawns.AllPawns.FindAll(p => ChildrenUtility.GetLifestageType(p) <= LifestageType.Toddler);
        }

        public override bool ShouldSkip(Pawn pawn, bool forced = false) {
            return !pawn.Drafted;
        }

        public override bool HasJobOnThing(Pawn pawn, Thing t, bool forced = false) {
            Pawn baby = (Pawn)t;

            // Too old to be carried
            if (ChildrenUtility.GetLifestageType(baby) > LifestageType.Toddler) {
                return false;
            }

            // Target already carried
            if (CarryUtility.IsCarried((Pawn)t)) {
                return false;
            }

            // Already carrying someone
            if (pawn.TryGetComp<CompCarryChild>().Carried != null) {
                return false;
            }

            if (baby.Faction == null || baby.Faction != Faction.OfPlayer) {
                return false;
            }

            if (baby.Drafted) {
                return false;
            }

            return true;
        }

        public override Job JobOnThing(Pawn pawn, Thing t, bool forced = false) {
            Pawn baby = (Pawn)t;
            Job job = JobMaker.MakeJob(CnpJobDefOf.CarryBaby);
            job.targetA = baby;
            return job;
        }
    }
}
