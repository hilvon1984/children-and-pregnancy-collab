﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using System.Diagnostics;
    using RimWorld;
    using RimWorldChildren.API;
    using Verse;
    using Verse.AI;

    public class WorkGiver_PlayWithBaby : WorkGiver_Scanner {
        public override PathEndMode PathEndMode {
            get {
                return PathEndMode.Touch;
            }
        }

        public override ThingRequest PotentialWorkThingRequest {
            get {
                return ThingRequest.ForGroup(ThingRequestGroup.Pawn);
            }
        }

        public override bool HasJobOnThing (Pawn pawn, Thing t, bool forced = false) {
            if (!(t is Pawn baby) || baby == pawn) {
                return false;
            }

            if (!baby.RaceProps.Humanlike || !ChildrenUtility.RaceUsesChildren(baby) || ChildrenUtility.GetLifestageType(baby) > LifestageType.Toddler) {
                return false;
            }

            if (!baby.InBed() || !baby.Awake()) {
                return false;
            }

            if (baby.needs?.joy == null || baby.needs.joy.CurLevelPercentage > 0.9f) {
                return false;
            }

            if (!pawn.CanReserveAndReach(t, PathEndMode.ClosestTouch, Danger.None, 1, -1, null, forced)) {
                return false;
            }

            return true;
        }

        public override Job JobOnThing (Pawn pawn, Thing t, bool forced = false) {
            Pawn pawn2 = (Pawn)t;
            if (pawn2 != null) {
                return new Job (DefDatabase<JobDef>.GetNamed ("PlayWithBaby")) {
                    targetA = pawn2,
                };
            }

            return null;
        }
    }

    public class JobDriver_PlayWithBaby : JobDriver {
        private const TargetIndex BabyInd = TargetIndex.A;

        private const TargetIndex ChairInd = TargetIndex.B;

        private Pawn Baby {
            get {
                return (Pawn)job.GetTarget(TargetIndex.A).Thing;
            }
        }

        private Thing Chair {
            get {
                return job.GetTarget(TargetIndex.B).Thing;
            }
        }

        public override bool TryMakePreToilReservations(bool errorOnFailed) {
            return this.pawn.Reserve(this.Baby, this.job, 1, -1, null);
        }

        [DebuggerHidden]
        protected override IEnumerable<Toil> MakeNewToils() {
            this.FailOnDespawnedNullOrForbidden(TargetIndex.A);
            this.FailOn(() => !Baby.InBed() || !Baby.Awake());
            if (Chair != null) {
                this.FailOnDespawnedNullOrForbidden(TargetIndex.B);
            }

            yield return Toils_Reserve.Reserve(TargetIndex.A);
            if (Chair != null) {
                yield return Toils_Reserve.Reserve (TargetIndex.B);
                yield return Toils_Goto.GotoThing(TargetIndex.B, PathEndMode.OnCell);
            }
            else {
                yield return Toils_Goto.GotoThing(TargetIndex.A, PathEndMode.InteractionCell);
            }

            yield return Toils_Interpersonal.WaitToBeAbleToInteract(pawn);
            yield return new Toil {
                tickAction = delegate {
                    Baby.needs.joy.GainJoy(job.def.joyGainRate * 0.000144f, job.def.joyKind);
                    if (pawn.IsHashIntervalTick(320)) {
                        InteractionDef intDef = (Rand.Value >= 0.8f) ? DefDatabase<InteractionDef>.GetNamed("BabyGames", true) : InteractionDefOf.Chitchat;
                        pawn.interactions.TryInteractWith(Baby, intDef);
                    }

                    pawn.rotationTracker.FaceCell(Baby.Position);

                    pawn.GainComfortFromCellIfPossible();
                    if (Baby.needs.joy.CurLevelPercentage > 0.99f) {
                        pawn.jobs.EndCurrentJob(JobCondition.Succeeded);
                    }
                },
                socialMode = RandomSocialMode.Off,
                defaultCompleteMode = ToilCompleteMode.Delay,
                defaultDuration = job.def.joyDuration,
            };
        }
    }
}