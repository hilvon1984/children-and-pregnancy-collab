﻿namespace RimWorldChildren {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using RimWorld;
    using UnityEngine;
    using Verse;
    using RimWorldChildren.API;

    /// <summary>
    /// This component will keep track of a pawn's growth from birth to "adult."
    /// By using a component instead of a hediff, we have the added advantage of tracking state
    /// in a less volatile manner, as our state object is no longer subject to other mods
    /// removing the state hediff. If our expected hediffs are removed, this comp can readd them as necessary.
    ///
    /// potentialTraits are traits that have been submitted by one mod or another to be randomly selected from
    /// when the pawn matures to "adult" stage.
    ///
    /// requiredTraits are traits that have been submitted by one mod or another to be forced upon the pawn
    /// when the pawn matures. These will still respect normal trait rules, however. Required traits will
    /// be added to the pawn starting with the highest weight.
    ///
    /// geneticTraits are only applied to a pawn at stage 0 - when they are first born. These traits
    /// are applied and never saved.
    ///
    /// adultBodyType is the BodyTypeDef this pawn generated with. When the pawn reaches maturity, it will
    /// transition to using this bodyTypeDef
    /// </summary>
    public class LifecycleComp : ThingComp {
        private int growthStage;
        private int acceleratedFactor;
        private bool colonyBorn;
        private bool initialized;
        public TraitPool potentialTraits;
        public TraitPool requiredTraits;
        private TraitPool geneticTraits;
        private BodyTypeDef adultBodyType;
        private RacialLifestageDef lifecycleDef;
        private MeshsetPackage meshes;
        private bool graphicUpdateDisabledThisTick = false;


        /// <summary>
        /// Gets the LifecycleDef being used to define how this pawn operates
        /// </summary>
        public RacialLifestageDef LifecycleDef {
            get {
                if (lifecycleDef == null) {
                    lifecycleDef = DefDatabase<RacialLifestageDef>.GetNamed(parent.def.defName + "_Lifestages", false);
                    if (lifecycleDef == null) {
                        // We're making the assumption that this comp will not be added to a race that does not qualify
                        // If this code is executing, it's assumed that at a minimum, this pawn's race is sufficiently humanlike
                        lifecycleDef = DefDatabase<RacialLifestageDef>.GetNamed("Default_Lifestages", true);
                    }
                }

                return lifecycleDef;
            }
        }

        /// <summary>
        /// Gets the collection of meshes to be used for rendering
        /// </summary>
        public MeshsetPackage Meshsets {
            get {
                if (meshes == null) {
                    meshes = new MeshsetPackage();
                    SetMeshesForLifestage();
                }

                return meshes;
            }
        }

        public CompProperties_LifeCycle Props {
            get {
                return (CompProperties_LifeCycle)props;
            }
        }

        public int CurrentLifestageIndex {
            get => growthStage;
        }

        public LifecycleStage CurrentLifestage {
            get {
                if (!initialized) {
                    Initialize(false);
                }

                return LifecycleDef?.lifeStages[CurrentLifestageIndex];
            }
        }

        /// <summary>
        /// Gets the lifestage that most closely matches the pawn's current status
        /// </summary>
        private int MostAptLifestage {
            get {
                List<LifecycleStage> stgs = LifecycleDef.lifeStages.Where(s => s.lifeStageDef == ((Pawn)parent).ageTracker.CurLifeStage).ToList();
                int mostApt = LifecycleDef.lifeStages.FindIndex(s => s == stgs[0]);
                if (stgs.Count > 1) {
                    foreach (LifecycleStage stg in stgs) {
                        if (stg.minAge <= ((Pawn)parent).ageTracker.AgeBiologicalYears) {
                            mostApt = LifecycleDef.lifeStages.FindIndex(s => s == stg);
                        }
                    }
                }

                return mostApt;
            }
        }

        public override void PostExposeData() {
            Scribe_Values.Look(ref growthStage, "growthStage", 0);
            Scribe_Values.Look(ref initialized, "growthInitialized", false);
            Scribe_Values.Look(ref colonyBorn, "colonyBorn", false);
            Scribe_Deep.Look(ref potentialTraits, "potentialTraits");
            Scribe_Deep.Look(ref requiredTraits, "requiredTraits");
            Scribe_Defs.Look(ref adultBodyType, "adultBodyType");

            base.PostExposeData();
        }

        /// <summary>
        /// Setup or reinitialize pawn growth settings
        /// </summary>
        public void Initialize(bool reinitialize) {
            CLog.Message("Executing GrowingComp Initialize: reinit = " + reinitialize);

            if (!initialized || reinitialize) {
                Pawn pawn = (Pawn)parent;
                initialized = true;
                CLog.Message("pawn.story.bodyType " + pawn.story.bodyType);
                adultBodyType = pawn.story.bodyType;
                potentialTraits = new TraitPool();
                requiredTraits = new TraitPool();
                geneticTraits = Props.geneticTraits ?? new TraitPool();
                growthStage = MostAptLifestage;
                CLog.Message("Most Apt Stage for " + pawn.Name + ": " + growthStage);
                colonyBorn = Props.ColonyBorn;
                if (reinitialize) {
                    // Clean out randomly generated drug addictions
                    pawn.health.hediffSet.hediffs.RemoveAll(hediff => !GenerationUtility.HediffWhitelist.Contains(hediff.def));
                }

                DestroyHediffs();
                GrowToStage(growthStage);
            }
        }

        /// <summary>
        /// Exposed to allow callers to forcefully update the tracker in the rare case of needing
        /// information on age up immediately.
        /// <paramref name="graphicUpdateDisabledThisTick">Wheter or not to disable the graphic update when updating the comp.</paramref>
        /// </summary>
        public void ForceUpdate(bool graphicUpdateDisabledThisTick) {
            this.graphicUpdateDisabledThisTick = graphicUpdateDisabledThisTick;
            CompTickRare();
        }

        /// <summary>
        /// This method will attempt to actually apply traits from a pool to the pawn.
        /// Traits will be applied up to a provided limit, or up to the max traits parameter
        /// defined by the player settings.
        /// Traits will be applied based on their weight.
        /// If the pawn has more trait slots than there are traits in traitPool, all traits will
        /// be applied.
        ///
        /// Normal trait rules apply, and acquiring one random trait may make the pawn uneligible for another.
        /// </summary>
        /// <param name="traitPool">The pool of traits to apply to the pawn</param>
        /// <param name="random">Whether to apply traits based on a weighted random selection, or to apply them
        ///                      in order based on max weight</param>
        /// <param name="randomNumber">Whether to randomize the number of traits applied</param>
        /// <param name="limit">The maximum number of traits to apply from this pool. Null by default.</param>
        /// <returns>The provided traitPool minus any traits that were removed or applied</returns>
        private TraitPool TryApplyTraits(TraitPool traitPool, bool random, bool randomNumber, int? limit = null) {
            Pawn pawn = (Pawn)parent;
            int currentTraits = pawn.story.traits.allTraits.Count;
            int traitsToApply = ChildrenAndPregnancy.Settings.maxTraits - currentTraits;
            if (limit != null && traitsToApply > limit) {
                traitsToApply = limit.Value;
            }

            if (randomNumber && traitsToApply > 1) {
                traitsToApply = Rand.RangeInclusive(1, traitsToApply);
            }

            int applied = 0;
            if (traitPool != null) {
                while (applied < traitsToApply && traitPool?.Count > 0) {
                    AcquirableTrait potentialTrait = null;
                    if (random) {
                        potentialTrait = traitPool.RandomElementByWeight(tr => tr.Weight);
                    } else {
                        potentialTrait = traitPool.MaxBy(trait => trait.Weight);
                    }

                    if (potentialTrait != null
                               && !pawn.story.traits.allTraits.Any(existingTrait => existingTrait.def.ConflictsWith(potentialTrait.TraitDef))
                               && !pawn.story.traits.HasTrait(potentialTrait.TraitDef)) {
                        Trait newTrait = new Trait(potentialTrait.TraitDef, potentialTrait.Degree, true);
                        pawn.story.traits.GainTrait(newTrait);
                        CLog.Message("Applying trait " + newTrait.Label);
                        applied++;
                    }

                    // Either we added it successfully or it wasn't valid, so remove it in all cases
                    traitPool.Remove(potentialTrait);
                }
            }

            return traitPool;
        }

        /// <summary>
        /// Returns a random non-birthable AcquirableTrait
        /// </summary>
        /// <param name="pawn">The pawn that the traits will be applied to</param>
        private AcquirableTrait GetRandomTrait(Pawn pawn) {
            // Get a list of all genetic traits that aren't already in the baby's list
            IEnumerable<TraitDef> geneticdefs = geneticTraits.Select(a => a.TraitDef);
            IEnumerable<TraitDef> birthableTraits = PregnancyUtility.GetGeneticTraits().Where(def => !geneticdefs.Contains(def));

            // Select a random trait and return it
            TraitDef select = birthableTraits.RandomElement();
            return new AcquirableTrait(select, select.degreeDatas.RandomElement().degree, Rand.Range(0.5f, 1));
        }

        /// <summary>
        /// This method will update an infant pawn's mood/hediff based on predetermined criteria
        /// </summary>
        protected void UpdateInfantMood() {
            Pawn pawn = (Pawn)parent;
            if ((pawn.needs.food != null && (pawn.needs.food.CurLevelPercentage < pawn.needs.food.PercentageThreshHungry ||
                    pawn.needs.joy?.CurLevelPercentage < 0.1f)) || (pawn.health.HasHediffsNeedingTend() && !pawn.health.hediffSet.HasHediff(ChildHediffDefOf.UnhappyBaby))) {
                pawn.health.AddHediff(ChildHediffDefOf.UnhappyBaby, null, null);
            } else if (pawn.health.hediffSet.HasHediff(ChildHediffDefOf.UnhappyBaby)) {
                pawn.health.hediffSet.hediffs.Remove(pawn.health.hediffSet.GetFirstHediffOfDef(ChildHediffDefOf.UnhappyBaby));
            }
        }

        /// <summary>
        /// This method will handle all routine mood/hediff updates for this pawn.
        /// </summary>
        public void UpdateMood() {
            if (ChildrenUtility.GetLifestageType((Pawn) parent) <= LifestageType.Toddler) {
                UpdateInfantMood();
            }
        }

        /// <summary>
        /// When this child has grown fully, we no longer need this component and may therefor
        /// destroy it forever. This cleanup step will remove unnecessary ticks as well.
        /// </summary>
        public void Destroy() {
            DestroyHediffs();
        }

        /// <summary>
        /// Remove any hediffs that were added in order to simulate child growth.
        /// Birth defects and similar hediffs will not be removed.
        /// </summary>
        public void DestroyHediffs() {
            Pawn pawn = (Pawn)parent;

            if (pawn.health.hediffSet.HasHediff(ChildHediffDefOf.UnhappyBaby)) {
                pawn.health.hediffSet.hediffs.Remove(pawn.health.hediffSet.GetFirstHediffOfDef(ChildHediffDefOf.UnhappyBaby));
            }
        }

        /// <summary>
        /// Adds random traits to the geneticTraits pool prior to birth
        /// </summary>
        private void RandomizeGeneticTraits() {
            Pawn pawn = (Pawn)parent;
            AcquirableTrait s = ChildrenUtility.GetSexuality(pawn);
            if (s != null) {
                requiredTraits.Add(s);
            }

            // Add random traits to genetic traits pool
            int rand = ChildrenAndPregnancy.Settings.randTraits;
            if (rand > 0) {
                for (int i = 0; i <= rand; i++) {
                    geneticTraits.Add(GetRandomTrait(pawn));
                }
            }
        }

        /// <summary>
        /// This method will setup all appropriate hediffs and other settings for a given life stage index.
        /// </summary>
        /// <param name="stage">The new lifestage index</param>
        public void GrowToStage(int stage) {
            Pawn pawn = (Pawn)parent;
            growthStage = stage;

            SetMeshesForLifestage();

            // Refresh pawn capacities in case this lifestage has new capmods.
            pawn.health.capacities.Notify_CapacityLevelsDirty();
            pawn.health.CheckForStateChange(default, null);

            if (CurrentLifestage.backstoryIdentifier != null) {
                pawn.story.childhood = BackstoryDatabase.allBackstories[CurrentLifestage.backstoryIdentifier];
                ChildrenUtility.RefreshDisabledWorkTypes(pawn);
            }

            ForceRefreshBodyType(false);

            if (growthStage != LifecycleDef.lifeStages.Count - 1) {
                if (growthStage == 0) {
                    RandomizeGeneticTraits();
                    TryApplyTraits(geneticTraits, true, true, ChildrenAndPregnancy.Settings.maxTraits - 1);
                }
                else if (CurrentLifestage.maturity && colonyBorn) {
                    GenMilestoneTrait.GetMilestoneTraits(pawn).ToList().ForEach(trait => potentialTraits.Add(trait));
                    TryApplyTraits(requiredTraits, false, false);
                    TryApplyTraits(potentialTraits, true, false);
                    DestroyHediffs();
                }

                PortraitsCache.SetDirty(pawn);

                // Necessary to prevent resource access cross thread
                if (!graphicUpdateDisabledThisTick) {
                    LongEventHandler.ExecuteWhenFinished(delegate {
                        pawn.Drawer.renderer.graphics.ResolveAllGraphics();
                    });
                }

                graphicUpdateDisabledThisTick = false;
            }
            else {
                DestroyHediffs();
            }
        }

        public void ForceRefreshBodyType() {
            ForceRefreshBodyType(true);
        }

        /// <summary>
        /// Immediately resets the body type of this pawn based on various factors.
        /// If compatibility rendering is enabled, this will attempt to set the curreny body
        /// type first to a "thin" variant, then (if not found) to their adult variant.
        /// Otherwise applies the relevant lifestage variant, if any exists.
        /// </summary>
        private void ForceRefreshBodyType(bool refreshGraphics) {
            Pawn pawn = (Pawn)parent;
            if (CurrentLifestage.maturity) {
                if (adultBodyType != null) {
                    CLog.Message(pawn.Name + " Setting body type to adult body type " + adultBodyType);
                    pawn.story.bodyType = adultBodyType;
                }

                return;
            }

            //TODO: Thin first, then adult? 
            if (CurrentLifestage.graphics != null && CurrentLifestage.graphics.alternativeRendering) {
                CLog.Message(pawn.Name + " Setting body type to Thin for compatibility");
                pawn.story.bodyType = DefDatabase<BodyTypeDef>.GetNamed("Thin", true);
            } else if (CurrentLifestage.graphics?.bodyType != null) {
                CLog.Message(pawn.Name + " Setting body type to " + CurrentLifestage.graphics.bodyType);
                pawn.story.bodyType = CurrentLifestage.graphics.bodyType;
            }

            if (refreshGraphics) {
                LongEventHandler.ExecuteWhenFinished(delegate {
                    pawn.Drawer.renderer.graphics.ResolveAllGraphics();
                });
            }
        }

        /// <summary>
        /// Setup all of our mesh sets so we do not have to repeatedly retrieve them during rendering
        /// </summary>
        private void SetMeshesForLifestage() {
            if (CurrentLifestage.graphics != null) {
                LongEventHandler.ExecuteWhenFinished(delegate {
                    // 1.5 is the default scale of these meshes
                    Meshsets.headMeshSet = new GraphicMeshSet(1.5f * CurrentLifestage.graphics.headScale.x, 1.5f * CurrentLifestage.graphics.headScale.y);
                    Meshsets.bodyMeshSet = new GraphicMeshSet(1.5f * CurrentLifestage.graphics.bodyScale.x, 1.5f * CurrentLifestage.graphics.bodyScale.y);
                    Meshsets.hairMeshSet = new GraphicMeshSet(1.5f * CurrentLifestage.graphics.hairScale.x, 1.5f * CurrentLifestage.graphics.hairScale.y);
                });
            }
        }

        /// <summary>
        /// Adds some debugging info the the pawn stat screen when debugging is enabled.
        /// </summary>
        public override IEnumerable<StatDrawEntry> SpecialDisplayStats() {
            if (ChildrenAndPregnancy.Settings.debugLogging) {
                yield return new StatDrawEntry(StatCategoryDefOf.PawnMisc, "[CNP] CurrentLifestageIndex", CurrentLifestageIndex.ToString(), CurrentLifestageIndex.ToString(), 1);
                yield return new StatDrawEntry(StatCategoryDefOf.PawnMisc, "[CNP] growthStage", growthStage.ToString(), growthStage.ToString(), 1);
                yield return new StatDrawEntry(StatCategoryDefOf.PawnMisc, "[CNP] Lifestage", ((Pawn)parent).ageTracker.CurLifeStageIndex.ToString(), "", 1);
            }
        }

        public override void CompTickRare() {
            Pawn pawn = (Pawn)parent;
            if (parent.Spawned && !pawn.Dead) {
                UpdateMood();
                if (growthStage < MostAptLifestage) {
                    GrowToStage(MostAptLifestage);
                }

                if (ChildrenAndPregnancy.Settings.acceleratedGrowthEnabled && pawn.ageTracker.AgeBiologicalYears < ChildrenAndPregnancy.Settings.acceleratedGrowthEndAge) {
                    acceleratedFactor = ChildrenUtility.SettingAcceleratedFactor(CurrentLifestage.lifestageType);
                    pawn.ageTracker.AgeBiologicalTicks += (long)acceleratedFactor * 250;
                }
            }
        }
    }

    /// <summary>
    /// This properties class is necessary to reference LifeCycleComp when adding the comp to a child, but
    /// is otherwise not used.
    /// geneticTraits may be set before a call to initialize, and the traits will be applied
    /// to a new born pawn on initialization based on random weight.
    /// </summary>
    public class CompProperties_LifeCycle : CompProperties {
        public bool ColonyBorn;
        public TraitPool geneticTraits;

        public CompProperties_LifeCycle() {
            this.compClass = typeof(LifecycleComp);
        }

        public CompProperties_LifeCycle(Type compClass) : base(compClass) {
            this.compClass = compClass;
        }
    }
}
