﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimWorld;
using UnityEngine;

namespace RimWorldChildren {

    /// <summary>
    /// Model to contained definitions for graphics handling
    /// </summary>
    public class GraphicsRuleset {

        // Optional: The bodytype to switch to at this stage
        public BodyTypeDef bodyType;

        // Optional x,y offset for hair.
        public Vector2 hairOffset = Vector2.zero;

        // Optional x,y scale for hair
        public Vector2 hairScale = Vector2.one;

        // Optional x,y offset for hats when facing north/south. Hat scale is controlled by hair scale.
        public Vector2 hatOffsetNS = Vector2.zero;

        // Optional x,y offset for hats when facing east/west. Hat scale is controlled by hair scale.
        public Vector2 hatOffsetEW = Vector2.zero;

        // Optional: The hair graphic to switch to at this stage
        public string hairGraphicPath;

        // Optional: Whether to use a different hair path for male and female
        public bool genderedHair = false;

        // Optional x,y offset for clothing. Applied when facing north/south
        public Vector2 clothingOffsetNS = Vector2.zero;

        // Optional x,y offset for clothing. Applied when facing east/west
        public Vector2 clothingOffsetEW = Vector2.zero;

        // Optional x,y scale for clothing
        public Vector2 clothingScale = Vector2.one;

        // Optional: Body grapic scale
        public Vector2 bodyScale = Vector2.one;

        // Optional: Headscale to be applied whether overriding graphic or not
        public Vector2 headScale = Vector2.one;

        // Optional x,y offset for hair.
        public Vector2 headOffset = Vector2.one;

        // Optional: The graphic path of a head to be used for this stage
        public string headGraphicPath;

        // Optional: a collection of Alien Races body addons to be resized/adjusted for this lifestage
        public List<ScalableBodyAddon> bodyAddons;

        // Optional (default: true): Whether or not to render the head, in case the body provides the head graphic as well
        public bool renderHead = true;

        // Optional: The body graphic to switch to at this stage
        public string bodyGraphicsPath;

        // Optional: Use this body type when determining clothes for this pawn
        public BodyTypeDef clothesBodyType;

        // Optional: When true, most rendering changes for bodys will be replaced by a simpler body-scale reduction to improve un-patched compatibility
        public bool alternativeRendering = false;

    }
}
